<?php

Class Order_item_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("order_items");

    }

    public function getOrderItems($where = false, $system_language_code = 'EN')
    {
        $this->db->select('order_items.*, products_text.Title as ProductTitle, products.*');
        $this->db->from('order_items');
        $this->db->join('products', 'products.ProductID = order_items.ProductID');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = products_text.SystemLanguageID', 'Left');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        if ($where)
        {
            $this->db->where($where);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }


    public function getProductOrderCount($ProductID)
    {
        $this->db->select('Count(order_items.ProductID) as ProductPurchaseCount');
        $this->db->from('order_items');
        
        $this->db->where('order_items.ProductID',$ProductID);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            $result =  $result->result_array();
            return $result[0]['ProductPurchaseCount'];
        } else {
            return (int) 0;
        }
    }

}
    
    
    