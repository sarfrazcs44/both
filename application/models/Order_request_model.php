<?php

Class Order_request_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("orders_requests");

    }

    public function getOrdersRequests($where = false, $system_language_code = 'EN', $limit = false, $start = 0, $sort_by = 'orders_requests.OrderRequestID', $sort_as = 'DESC')
    {
        $this->db->select("orders_requests.*,
                            bu.UserID as BoothID, 
                            bu.CompressedBoothImage as BoothImage, 
                            bu.BoothUserName as BoothUserName, 
                            bu.Email as BoothEmail, 
                            bu.Mobile as BoothMobile, 
                            but.BoothName, 
                            u.UserID as UserID, 
                            u.CompressedImage as UserImage, 
                            u.UserName as UserName, 
                            u.Email as UserEmail, 
                            u.Mobile as UserMobile,
                            u.Gender as UserGender,
                            u.IsEmailVerified,
                            u.IsMobileVerified,
                            u.OnlineStatus,
                            ut.FullName, 
                            bu.BoothType, 
                            IF('$system_language_code' = 'AR', order_statuses.OrderStatusAr , order_statuses.OrderStatusEn) as OrderStatus,
                            IF('$system_language_code' = 'AR', order_cancellation_reasons.CancellationReasonAr , order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason,
                            buct.Title as BoothCityTitle, 
                            uct.Title as UserCityTitle, 
                            orders.CreatedAt as OrderReceivedAt,
                            user_addresses.AddressTitle, 
                            user_addresses.RecipientName, 
                            user_addresses.Email as AddressEmail, 
                            user_addresses.Mobile as AddressMobile, 
                            user_addresses.Gender as AddressGender, 
                            user_addresses.ApartmentNo, 
                            user_addresses.Address1, 
                            user_addresses.Address2, 
                            user_addresses.City as AddressCity, 
                            user_addresses.Latitude as AddressLatitude, 
                            user_addresses.Longitude as AddressLongitude, 
                            user_addresses.IsDefault as AddressIsDefault, 
                            user_customization.VatPercentage,
                            bucc.CountryShortName as BoothCountryShortName,
                            bucc.Currency as BoothCurrency,
                            bucct.Title as BoothCountryTitle");
        $this->db->from('orders_requests');
        $this->db->join('order_statuses', 'orders_requests.OrderStatusID = order_statuses.OrderStatusID');
        $this->db->join('order_cancellation_reasons', 'orders_requests.OrderCancellationReasonID = order_cancellation_reasons.OrderCancellationReasonID', 'LEFT');
       // $this->db->join('users bu', 'orders_requests.BoothID = bu.UserID AND bu.PackageExpiry >= CURDATE()');
        $this->db->join('users bu', 'orders_requests.BoothID = bu.UserID');
        $this->db->join('users_text but', 'bu.UserID = but.UserID AND but.SystemLanguageID = 1');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->join('users u', 'orders.UserID = u.UserID');
        $this->db->join('users_text ut', 'u.UserID = ut.UserID AND ut.SystemLanguageID = 1');
        $this->db->join('user_addresses', 'orders.AddressID = user_addresses.AddressID', 'LEFT');
        $this->db->join('user_customization', 'bu.UserID = user_customization.UserID');
        $this->db->join('cities buc', 'bu.CityID = buc.CityID', 'LEFT');
        $this->db->join('cities_text buct', 'buc.CityID = buct.CityID');
        $this->db->join('cities uc', 'u.CityID = uc.CityID', 'LEFT');
        $this->db->join('cities_text uct', 'uc.CityID = uct.CityID');
        $this->db->join('countries bucc', 'buc.CountryID = bucc.CountryID', 'LEFT');
        $this->db->join('countries_text bucct', 'bucc.CountryID = bucct.CountryID');
        $this->db->join('system_languages slbuct', 'buct.SystemLanguageID = slbuct.SystemLanguageID');
        $this->db->join('system_languages slbucct', 'bucct.SystemLanguageID = slbucct.SystemLanguageID');
        $this->db->join('system_languages sluct', 'uct.SystemLanguageID = sluct.SystemLanguageID');
        $this->db->where('slbuct.ShortCode', $system_language_code);
        $this->db->where('slbucct.ShortCode', $system_language_code);
        $this->db->where('sluct.ShortCode', $system_language_code);
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('orders_requests.OrderRequestID');
        $this->db->order_by($sort_by, $sort_as);

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getOrderRequestsForCronjob($where = false)
    {
        $this->db->select("orders_requests.*, orders.*");
        $this->db->from('orders_requests');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        if ($where)
        {
            $this->db->where($where);
        }
        return $this->db->get()->result();
    }

}
    
    
    