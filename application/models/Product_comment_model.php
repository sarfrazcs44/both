<?php

Class Product_comment_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("product_comments");

    }

    public function getProductComments($where = false, $system_language_code = 'EN', $sort_by = 'product_comments.ProductCommentID', $sort_as = 'ASC', $limit = false, $start = 0)
    {
        $this->db->select('product_comments.*, users.UserID, users.CompressedImage,users.CompressedBoothImage, users.UserName, users.BoothUserName, users_text.FullName, users_text.BoothName, cities_text.Title as CityTitle');
        $this->db->from('product_comments');
        $this->db->join('users', 'product_comments.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID');
        $this->db->join('system_languages', 'cities_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where($where);
        $this->db->order_by($sort_by, $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        return $result->result();
    }

    public function getProductCommentsCount($ProductID)
    {
        $this->db->select('COUNT(ProductID) as Total');
        $this->db->from('product_comments');
        $this->db->where('ProductID', $ProductID);
        $result = $this->db->get();
        return $result->row();
    }


}