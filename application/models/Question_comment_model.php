<?php

Class Question_comment_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("question_comments");

    }

    public function getQuestionComments($where = false, $system_language_code = 'EN', $sort_by = 'question_comments.QuestionCommentID', $sort_as = 'ASC', $limit = false, $start = 0)
    {
        $this->db->select('question_comments.*, users.UserID, users.CompressedImage,users.CompressedBoothImage, users.UserName, users.BoothUserName, users_text.FullName, users_text.BoothName, cities_text.Title as CityTitle');
        $this->db->from('question_comments');
        $this->db->join('users', 'question_comments.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID');
        $this->db->join('system_languages', 'cities_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where($where);
        $this->db->order_by($sort_by, $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        return $result->result();
    }

    public function getQuestionCommentsCount($QuestionID)
    {
        $this->db->select('COUNT(QuestionCommentID) as Total');
        $this->db->from('question_comments');
        $this->db->where('QuestionID', $QuestionID);
        $result = $this->db->get();
        return $result->row();
    }

}