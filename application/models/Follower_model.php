<?php

Class Follower_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_followers");

    }

    public function getFollowing($id, $type, $start = false, $limit = false, $search = false, $lang = 'EN')
    {
        $todaydate = date('Y-m-d');
        $query = "SELECT users.*,users_text.*,cities_text.Title as CityTitle, countries.Currency, countries.CurrencySymbol FROM user_followers
 JOIN users ON user_followers.Following = users.UserID
  JOIN users_text ON users.UserID = users_text.UserID
   LEFT JOIN cities on users.CityID = cities.CityID
    JOIN cities_text ON cities.CityID = cities_text.CityID
    LEFT JOIN countries on cities.CountryID = countries.CountryID
    JOIN countries_text ON countries.CountryID = countries_text.CountryID
     JOIN system_languages ON system_languages.SystemLanguageID = cities_text.SystemLanguageID
     JOIN system_languages slpt ON slpt.SystemLanguageID = countries_text.SystemLanguageID
      WHERE user_followers.Follower = " . $id . " AND user_followers.Type = '" . $type . "' AND users_text.SystemLanguageID = 1 AND system_languages.ShortCode = '$lang' AND slpt.ShortCode = '$lang' AND DATE(users.PackageExpiry) > '".$todaydate."' ";

        if ($search) {
            $query .= " AND users_text.FullName LIKE '%$search%' OR users_text.BoothName LIKE '%$search%' ";
        }

        if ($start && $limit) {
            $query .= " LIMIT $start,$limit";
        }
        $query = $this->db->query($query);
        //echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {
            return false;
        }

    }

    public function getTotalFollowing($id, $type)
    {
         $todaydate = date('Y-m-d');
        $sql = "Select Count(Follower) as Total from user_followers JOIN users ON user_followers.Following = users.UserID where Follower = " . $id . "  AND Type ='" . $type . "' AND DATE(users.PackageExpiry) > '".$todaydate."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0]['Total'];
        } else {
            return NULL;
        }

    }

    /*public function getFollower($id, $type, $start = false, $limit = false, $search = false)
    {
        $query = "Select users.*,users_text.* from user_followers JOIN users ON user_followers.Follower = users.UserID JOIN users_text ON users.UserID = users_text.UserID JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID where user_followers.Following = " . $id . " AND user_followers.Type = '" . $type . "' AND system_languages.ShortCode = 'EN'  ";

        if ($search) {
            $query .= " AND users_text.FullName LIKE '%$search%' OR users_text.BoothName LIKE '%$search%' ";
        }

        if ($start && $limit) {
            $query .= " LIMIT $start,$limit";
        }

        // echo $query;exit();

        $query = $this->db->query($query);


        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {

            return false;
        }

    }*/

    public function getFollower($id, $type, $start = false, $limit = false, $search = false, $lang = 'EN')
    {
        $query = "SELECT users.*,users_text.*,cities_text.Title as CityTitle, countries.Currency, countries.CurrencySymbol FROM user_followers
 JOIN users ON user_followers.Follower = users.UserID
  JOIN users_text ON users.UserID = users_text.UserID
   LEFT JOIN cities on users.CityID = cities.CityID
    JOIN cities_text ON cities.CityID = cities_text.CityID
    LEFT JOIN countries on cities.CountryID = countries.CountryID
    JOIN countries_text ON countries.CountryID = countries_text.CountryID
     JOIN system_languages ON system_languages.SystemLanguageID = cities_text.SystemLanguageID
     JOIN system_languages slpt ON slpt.SystemLanguageID = countries_text.SystemLanguageID
      WHERE user_followers.Following = " . $id . " AND user_followers.Type = '" . $type . "' AND users_text.SystemLanguageID = 1 AND system_languages.ShortCode = '$lang' AND slpt.ShortCode = '$lang' ";

        if ($search) {
            $query .= " AND users_text.FullName LIKE '%$search%' OR users_text.BoothName LIKE '%$search%' ";
        }

        if ($start && $limit) {
            $query .= " LIMIT $start,$limit";
        }
        $query = $this->db->query($query);
        // echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {
            return false;
        }

    }

    public function getTotalFollower($id, $type)
    {
        $sql = "Select Count(Following) as Total from user_followers where Following = " . $id . "  AND Type ='" . $type . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0]['Total'];
        } else {

            return NULL;
        }

    }

    public function getUserFollowedBooths($UserID)
    {
        $sql = "Select Count(Following) as Total from user_followers where Follower = " . $UserID . "  AND Type = 'booth'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return (int)$result[0]['Total'];
        } else {

            return 0;
        }
    }



}