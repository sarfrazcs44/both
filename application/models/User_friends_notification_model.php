<?php

Class User_friends_notification_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_friends_activities");
    }

    public function getActivities($where, $start = 0, $limit = false, $language = 'EN')
    {
        $this->db->select("user_friends_activities.*, liu.UserName as LoggedInUserName, liu.CompressedImage as LoggedInUserImage, users.UserName as UserName, users.BoothUserName,users.IsLikesPrivate,users.IsWishListPrivate,users.IsOrdersPrivate ,users.CompressedImage as UserImage,users.CompressedBoothImage as BoothImage, IF ('$language' = 'AR', user_friends_activities.NotificationTextAr, user_friends_activities.NotificationTextEn) AS NotificationText");
        $this->db->from('user_friends_activities');

        $this->db->join('users', 'user_friends_activities.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID', 'LEFT');

        $this->db->join('users liu', 'user_friends_activities.LoggedInUserID = liu.UserID', 'LEFT');
        $this->db->join('users_text liut', 'liu.UserID = liut.UserID', 'LEFT');

        $this->db->where($where);
        $this->db->group_by('user_friends_activities.UserFriendActivityID');
        $this->db->order_by('user_friends_activities.UserFriendActivityID', 'DESC');
        if ($limit)
        {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();
    }


}