<?php

Class User_notification_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_notifications");
    }

    public function getNotifications($where, $start = 0, $limit = false, $language = 'EN')
    {
        $this->db->select("user_notifications.*, liu.UserName as LoggedInUserName, liu.BoothUserName as LoggedInBoothUserName, liu.CompressedImage as LoggedInUserImage, users.UserName as UserName, users.BoothUserName as BoothUserName, users.CompressedImage as UserImage, users.CompressedBoothImage as BoothImage, IF ('$language' = 'AR', user_notifications.NotificationTextAr, user_notifications.NotificationTextEn) AS NotificationText");
        $this->db->from('user_notifications');

        $this->db->join('users', 'user_notifications.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID', 'LEFT');

        $this->db->join('users liu', 'user_notifications.LoggedInUserID = liu.UserID', 'LEFT');
        $this->db->join('users_text liut', 'liu.UserID = liut.UserID', 'LEFT');

        $this->db->where($where);
        $this->db->group_by('user_notifications.UserNotificationID');
        $this->db->order_by('user_notifications.UserNotificationID', 'DESC');
        if ($limit)
        {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();
    }


}