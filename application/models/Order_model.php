<?php

Class Order_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("orders");
    }

    public function getOrders($where = false, $limit = false, $start = 0, $sort_by = 'orders.OrderID', $sort_as = 'DESC')
    {
        $this->db->select('orders_requests.*, bu.UserID as BoothID, bu.CompressedBoothImage as BoothImage, bu.BoothUserName as BoothUserName, but.BoothName, u.UserID as UserID, u.CompressedImage as UserImage, u.UserName as UserName, ut.FullName');
        $this->db->from('orders_requests');
        $this->db->join('users bu', 'orders_requests.BoothID = bu.UserID');
        $this->db->join('users_text but', 'bu.UserID = but.UserID AND but.SystemLanguageID = 1');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->join('users u', 'orders.UserID = u.UserID');
        $this->db->join('users_text ut', 'u.UserID = ut.UserID AND ut.SystemLanguageID = 1');
        $this->db->join('user_addresses', 'orders.AddressID = user_addresses.AddressID', 'LEFT');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('orders.OrderID');
        $this->db->order_by($sort_by, $sort_as);

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }



    public function getOrdersRequest($where = false, $limit = false, $start = 0, $sort_by = 'orders.OrderID', $sort_as = 'DESC')
    {
        $this->db->select('orders_requests.*, bu.UserID as BoothID, bu.CompressedBoothImage as BoothImage, bu.BoothUserName as BoothUserName, but.BoothName, u.UserID as UserID, u.CompressedImage as UserImage, u.Email as UserEmail,u.UserName as UserName, ut.FullName');
        $this->db->from('orders_requests');
        $this->db->join('users bu', 'orders_requests.BoothID = bu.UserID');
        $this->db->join('users_text but', 'bu.UserID = but.UserID AND but.SystemLanguageID = 1');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->join('users u', 'orders.UserID = u.UserID');
        $this->db->join('users_text ut', 'u.UserID = ut.UserID AND ut.SystemLanguageID = 1');
        $this->db->join('user_addresses', 'orders.AddressID = user_addresses.AddressID', 'LEFT');
        if ($where) {
            $this->db->where($where);
        }
        //$this->db->group_by('orders.OrderID');
        $this->db->order_by($sort_by, $sort_as);

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getOrdersDetailWithCustomerDetail($order_id,$system_language_code = false,$where = false){
        
        $this->db->select('orders.*,order_items.*,order_items.Price as OrderPlaceTimePrice,orders_requests.OrderRequestID,products.*,products_text.*,users_text.FullName,users.Email as UserEmail');
        $this->db->from('orders');

        $this->db->join('orders_requests','orders_requests.OrderID = orders.OrderID');
        $this->db->join('order_items','order_items.OrderRequestID = orders_requests.OrderRequestID');
        

        $this->db->join('users','users.UserID = orders.UserID');
        $this->db->join('users_text','users_text.UserID = users.UserID');

      

        $this->db->join('products','products.ProductID = order_items.ProductID');
        $this->db->join('products_text','products_text.ProductID = products.ProductID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
        
        $this->db->where('orders.OrderID',$order_id);
        if($where){
          $this->db->where($where);  
        }
        if($system_language_code) {
                $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        
        
        $this->db->group_by('order_items.ProductID');
        
        return $this->db->get()->result_array();
        
        
    }


    public function getOrdersDetailForProductDelete($where = false,$system_language_code = false){
        
        $this->db->select('orders.*,order_items.*,order_items.Price as OrderPlaceTimePrice,orders_requests.OrderRequestID,products.*,products_text.*,users_text.FullName,users.Email as UserEmail');
        $this->db->from('orders');

        $this->db->join('orders_requests','orders_requests.OrderID = orders.OrderID');
        $this->db->join('order_items','order_items.OrderRequestID = orders_requests.OrderRequestID');
        

        $this->db->join('users','users.UserID = orders.UserID');
        $this->db->join('users_text','users_text.UserID = users.UserID');

      

        $this->db->join('products','products.ProductID = order_items.ProductID');
        $this->db->join('products_text','products_text.ProductID = products.ProductID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
        
        
        if($where){
          $this->db->where($where);  
        }
        if($system_language_code) {
                $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        
        
        $this->db->group_by('order_items.ProductID');
        
        return $this->db->get()->result_array();
        
        
    }

    public function getOrdersBK($where = false, $limit = false, $start = 0, $system_language_code = false, $sort = 'ASC')
    {
        $this->db->select('orders.*,users.Email,users.Mobile,users_text.FullName, cities_text.Title as UserCity, user_address.*');
        $this->db->from('orders');
        $this->db->join('orders_requests', 'orders.Status = orders_requests.OrderRequestID', 'LEFT');
        $this->db->join('user_address', 'orders.AddressID = user_address.AddressID', 'LEFT');
        $this->db->join('users', 'orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'left');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID', 'left');

        $this->db->join('tickets', 'orders.OrderID = tickets.OrderID', 'left');

        // driver details
        $this->db->join('users du', 'orders.DriverID = du.UserID', 'LEFT');
        $this->db->join('users_text dut', 'du.UserID = dut.UserID AND dut.SystemLanguageID = 1', 'LEFT');

        // Shipment Method
        $this->db->join('tax_shipment_charges', 'orders.ShipmentMethodID = tax_shipment_charges.TaxShipmentChargesID', 'LEFT');
        $this->db->join('tax_shipment_charges_text', 'tax_shipment_charges.TaxShipmentChargesID = tax_shipment_charges_text.TaxShipmentChargesID', 'LEFT');

        $this->db->join('system_languages as slct', 'slct.SystemLanguageID = cities_text.SystemLanguageID', 'Left');
        $this->db->join('system_languages as slact', 'slact.SystemLanguageID = act.SystemLanguageID', 'Left');
        $this->db->join('system_languages as sladt', 'sladt.SystemLanguageID = districts_text.SystemLanguageID', 'Left');
        $this->db->join('system_languages as sltsct', 'sltsct.SystemLanguageID = tax_shipment_charges_text.SystemLanguageID', 'Left');

        if ($system_language_code) {
            $this->db->where('slct.ShortCode', $system_language_code);
            $this->db->where('slact.ShortCode', $system_language_code);
            $this->db->where('sladt.ShortCode', $system_language_code);
            $this->db->where('sltsct.ShortCode', $system_language_code);
        } else {
            $this->db->where('slct.IsDefault', '1');
            $this->db->where('slact.IsDefault', '1');
            $this->db->where('sladt.IsDefault', '1');
            $this->db->where('sltsct.IsDefault', '1');
        }

        if ($where) {
            $this->db->where($where);
        }

        $this->db->group_by('orders.OrderID');
        $this->db->order_by('orders.OrderID', $sort);

        if ($limit) {
            $this->db->limit($limit, $start);
        }


        $result = $this->db->get();

        // echo $this->db->last_query();exit();
        return $result->result();


    }

    public function getOrderDetail($order_request_id, $system_language_code = 'EN')
    {
        $this->db->select("orders.*,orders_requests.*, IF('$system_language_code' = 'AR', order_statuses.OrderStatusAr , order_statuses.OrderStatusEn) as OrderStatus, IF('$system_language_code' = 'AR', order_cancellation_reasons.CancellationReasonAr , order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason");
        $this->db->from('orders');
        $this->db->join('orders_requests', 'orders.OrderID = orders_requests.OrderID');
        $this->db->join('order_statuses', 'orders_requests.OrderStatusID = order_statuses.OrderStatusID');
        $this->db->join('order_cancellation_reasons', 'orders_requests.OrderCancellationReasonID = order_cancellation_reasons.OrderCancellationReasonID', 'LEFT');
        $this->db->where('orders_requests.OrderRequestID', $order_request_id);
        return $this->db->get()->row_array();
    }

    public function getOrderItems($OrderRequestID, $system_language_code = 'EN')
    {
        $this->db->select('order_items.Quantity,order_items.Price as PlaceOrderPrice,products.*,products_text.*');
        $this->db->from('order_items');
        $this->db->join('products', 'products.ProductID = order_items.ProductID');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = products_text.SystemLanguageID', 'Left');
        $this->db->where('order_items.OrderRequestID', $OrderRequestID);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        
        return $this->db->get()->result_array();
    }

    public function getOrdersDetail($where = false, $system_language_code = 'EN')
    {
        $this->db->select('orders.*, users.UserID, users.Email, users.Mobile, users.Gender, users.IsEmailVerified, users.IsMobileVerified, users.OnlineStatus, users.CompressedImage, users.UserName, users.BoothUserName, users_text.FullName, users_text.BoothName, ua.AddressTitle, ua.RecipientName, ua.Email as RecipientEmail, ua.Mobile as RecipientMobile, ua.Gender as RecipientGender, ua.ApartmentNo, ua.Address1, ua.Address2, ua.City, ua.Latitude, ua.Longitude, orders.CreatedAt as OrderReceivedAt, cities_text.Title as UserCityTitle');
        $this->db->from('orders');
        $this->db->join('users', 'orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('user_addresses ua', 'orders.AddressID = ua.AddressID', 'LEFT');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID');
        $this->db->join('system_languages', 'cities_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        if ($where) {
            $this->db->where($where);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getTotalsales($year)
    {

        $this->db->select("YEAR(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d')) as SalesYear, MONTH(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d')) as SalesMonth, SUM(orders_requests.GrandTotal) As TotalSales");
        $this->db->from("orders_requests");
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->where("orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 4 OR orders_requests.OrderStatusID = 7");
        $this->db->where("YEAR(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d')) = ", $year);
        $this->db->group_by("YEAR(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d'))");
        $this->db->group_by("MONTH(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d'))");
        $this->db->order_by("YEAR(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d'))");
        $this->db->order_by("MONTH(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d'))");
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();
    }


    public function getTotalOrders($year)
    {
        $this->db->select("YEAR(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d')) as OrdersYear,MONTH(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d')) as OrderMonth,COUNT(orders_requests.OrderRequestID) As TotalOrders");
        $this->db->from("orders_requests");
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->where("YEAR(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d')) = ", $year);
        // $this->db->where("orders.Status", 4);
        $this->db->group_by("YEAR(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d'))");
        $this->db->group_by("MONTH(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d'))");
        $this->db->order_by("YEAR(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d'))");
        $this->db->order_by("MONTH(DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d'))");
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();
    }

}
