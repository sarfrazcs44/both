<?php
Class User_wishlist_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_wishlist");

    }

    public function getUserWishlistProducts($UserID, $system_language_code = 'EN')
    {
        $this->db->select('user_wishlist.*, products.ProductID, products_text.Title, users.UserID as BoothID, users_text.BoothName');
        $this->db->from('user_wishlist');
        $this->db->join('products', 'user_wishlist.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('users', 'products.UserID = users.UserID AND users.PackageExpiry >= CURDATE()');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('user_wishlist.UserID', $UserID);
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return array();
        }
    }

}