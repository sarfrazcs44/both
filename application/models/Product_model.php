<?php

Class Product_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("products");

    }

    public function getProducts($where = false, $system_language_code = 'EN', $limit = false, $start = 0, $sort_by = 'products_text.Title', $sort_as = 'ASC')
    {
        $this->db->select('users.*,users_text.*,products.*,products_text.*,cities_text.Title as CityName, ssct.Title as SubSubCategoryName, sct.Title as SubCategoryName, ct.Title as CategoryName');

        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');

        $this->db->join('users', 'users.UserID = products.UserID', 'LEFT');
        $this->db->join('users_text', 'users_text.UserID = users.UserID AND users_text.SystemLanguageID = 1');

        $this->db->join('cities', 'cities.CityID = users.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID');

        $this->db->join('countries', 'cities.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('countries_text', 'countries.CountryID = countries_text.CountryID');

        // sub sub category
        $this->db->join('categories ssc', 'products.CategoryID = ssc.CategoryID', 'LEFT');
        $this->db->join('categories_text ssct', 'ssc.CategoryID = ssct.CategoryID');

        // sub category
        $this->db->join('categories sc', 'ssc.ParentID = sc.CategoryID', 'LEFT');
        $this->db->join('categories_text sct', 'sc.CategoryID = sct.CategoryID');

        // category
        $this->db->join('categories c', 'sc.ParentID = c.CategoryID', 'LEFT');
        $this->db->join('categories_text ct', 'c.CategoryID = ct.CategoryID');

        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->join('system_languages slctt', 'cities_text.SystemLanguageID = slctt.SystemLanguageID');
        $this->db->join('system_languages slpt', 'countries_text.SystemLanguageID = slpt.SystemLanguageID');
        $this->db->join('system_languages slssct', 'ssct.SystemLanguageID = slssct.SystemLanguageID');
        $this->db->join('system_languages slsct', 'sct.SystemLanguageID = slsct.SystemLanguageID');
        $this->db->join('system_languages slct', 'ct.SystemLanguageID = slct.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('slctt.ShortCode', $system_language_code);
        $this->db->where('slpt.ShortCode', $system_language_code);
        $this->db->where('slssct.ShortCode', $system_language_code);
        $this->db->where('slsct.ShortCode', $system_language_code);
        $this->db->where('slct.ShortCode', $system_language_code);

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->group_by('products.ProductID');

        $this->db->order_by($sort_by, $sort_as);


        $result = $this->db->get();
        return $result->result();
    }


    public function getProductsForUserFollowCategory($where = false, $system_language_code = 'EN', $limit = false, $start = 0, $sort_by = 'products_text.Title', $sort_as = 'ASC', $user_selected_categories = false,$random = false)
    {
        $this->db->select('products.*,products_text.*,users_text.FullName,users_text.BoothName, cities_text.Title as CityName,users.UserName,users.BoothUserName,users.BoothImage,users.CompressedBoothImage,ssct.Title as SubSubCategoryName, sct.Title as SubCategoryName, ct.Title as CategoryName');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('users', 'users.UserID = products.UserID', 'LEFT');
        $this->db->join('users_text', 'users_text.UserID = users.UserID AND users_text.SystemLanguageID = 1');

        $this->db->join('cities', 'cities.CityID = users.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID');

        $this->db->join('countries', 'cities.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('countries_text', 'countries.CountryID = countries_text.CountryID');

        // sub sub category
        $this->db->join('categories ssc', 'products.CategoryID = ssc.CategoryID', 'LEFT');
        $this->db->join('categories_text ssct', 'ssc.CategoryID = ssct.CategoryID');

        // sub category
        $this->db->join('categories sc', 'ssc.ParentID = sc.CategoryID', 'LEFT');
        $this->db->join('categories_text sct', 'sc.CategoryID = sct.CategoryID');

        // category
        $this->db->join('categories c', 'sc.ParentID = c.CategoryID', 'LEFT');
        $this->db->join('categories_text ct', 'c.CategoryID = ct.CategoryID');

        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->join('system_languages slctt', 'cities_text.SystemLanguageID = slctt.SystemLanguageID');
        $this->db->join('system_languages slpt', 'countries_text.SystemLanguageID = slpt.SystemLanguageID');
        $this->db->join('system_languages slssct', 'ssct.SystemLanguageID = slssct.SystemLanguageID');
        $this->db->join('system_languages slsct', 'sct.SystemLanguageID = slsct.SystemLanguageID');
        $this->db->join('system_languages slct', 'ct.SystemLanguageID = slct.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }
        if ($user_selected_categories) {
            $this->db->where_in('ssc.ParentID', $user_selected_categories);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('slctt.ShortCode', $system_language_code);
        $this->db->where('slpt.ShortCode', $system_language_code);
        $this->db->where('slssct.ShortCode', $system_language_code);
        $this->db->where('slsct.ShortCode', $system_language_code);
        $this->db->where('slct.ShortCode', $system_language_code);

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->group_by('products.ProductID');
        if($random){
            $this->db->order_by('products.ProductID','DESC');
        }else{
            $this->db->order_by('products.IsPromotionApproved','DESC');
            $this->db->order_by($sort_by, $sort_as);
        }
       


        $result = $this->db->get();
        // echo $this->db->last_query();exit;
        return $result->result_array();
    }

    public function getProductsForUserFollowBooth($where = false, $system_language_code = 'EN', $limit = false, $start = 0, $sort_by = 'products_text.Title', $sort_as = 'ASC', $user_followed_booths = false)
    {
        $this->db->select('products.*,products_text.*,users_text.FullName,users_text.BoothName,cities_text.Title as CityName,users.UserName,users.BoothUserName,users.BoothImage,users.CompressedBoothImage,ssct.Title as SubSubCategoryName, sct.Title as SubCategoryName, ct.Title as CategoryName');

        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');

        $this->db->join('users', 'users.UserID = products.UserID', 'LEFT');
        $this->db->join('users_text', 'users_text.UserID = users.UserID AND users_text.SystemLanguageID = 1');

        $this->db->join('cities', 'cities.CityID = users.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID');

        $this->db->join('countries', 'cities.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('countries_text', 'countries.CountryID = countries_text.CountryID');

        // sub sub category
        $this->db->join('categories ssc', 'products.CategoryID = ssc.CategoryID', 'LEFT');
        $this->db->join('categories_text ssct', 'ssc.CategoryID = ssct.CategoryID');

        // sub category
        $this->db->join('categories sc', 'ssc.ParentID = sc.CategoryID', 'LEFT');
        $this->db->join('categories_text sct', 'sc.CategoryID = sct.CategoryID');

        // category
        $this->db->join('categories c', 'sc.ParentID = c.CategoryID', 'LEFT');
        $this->db->join('categories_text ct', 'c.CategoryID = ct.CategoryID');

        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->join('system_languages slctt', 'cities_text.SystemLanguageID = slctt.SystemLanguageID');
        $this->db->join('system_languages slpt', 'countries_text.SystemLanguageID = slpt.SystemLanguageID');
        $this->db->join('system_languages slssct', 'ssct.SystemLanguageID = slssct.SystemLanguageID');
        $this->db->join('system_languages slsct', 'sct.SystemLanguageID = slsct.SystemLanguageID');
        $this->db->join('system_languages slct', 'ct.SystemLanguageID = slct.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }

        $this->db->where_in('products.UserID', $user_followed_booths);

        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('slctt.ShortCode', $system_language_code);
        $this->db->where('slpt.ShortCode', $system_language_code);
        $this->db->where('slssct.ShortCode', $system_language_code);
        $this->db->where('slsct.ShortCode', $system_language_code);
        $this->db->where('slct.ShortCode', $system_language_code);

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->group_by('products.ProductID');
        $this->db->order_by('products.CreatedAt', 'DESC');
        $this->db->order_by('products.IsPromotionApproved','DESC');
        $this->db->order_by($sort_by, $sort_as);


        $result = $this->db->get();
        return $result->result_array();
    }


    public function getCountProducts($where = false, $system_language_code = 'EN', $limit = false, $start = 0)
    {
        $this->db->select('COUNT(products.ProductID) as Total');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);

        if ($limit) {
            $this->db->limit($limit, $start);
        }


        $result = $this->db->get();
        return $result->row();
    }

    public function getProductsOfCollection($product_ids, $system_language_code = 'EN', $limit = false, $start = 0, $count = false)
    {
        if ($count) {
            $this->db->select('COUNT(products.ProductID) as Total');

        } else {
            $this->db->select('products.*,products_text.*');

        }


        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where_in('products.ProductID', $product_ids);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        return $result->result();
    }

    public function getWishlistItems($UserID, $system_language_code = 'EN')
    {
        $this->db->select("user_wishlist.*,products_text.Title,products.ProductID,products.Price");
        $this->db->from('user_wishlist');
        $this->db->join('products', 'user_wishlist.ItemID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('user_wishlist.UserID', $UserID);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        return $result->result();
    }

    public function searchProducts($value, $system_language_code = 'EN')
    {
        $this->db->select('products.*,products_text.*');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->like('products_text.Title', $value, 'both');
        $this->db->where('products.IsCustomizedProduct', 0);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        return $result->result();
    }

    public function getProductDetail($product_id, $system_language_code = 'EN',$where = false)
    {
        $this->db->select('products.*,products_text.*');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');

        $this->db->join('users','users.UserID = products.UserID','left');

        if($where){
             $this->db->where($where);
        }

        $this->db->where('products.ProductID', $product_id);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        return $result->row();
    }

    public function getMostSellingProducts($system_language_code = 'EN', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('products.*, products_text.*, COUNT(order_items.ProductID) as OrderCount');
        $this->db->from('order_items');
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->having('OrderCount >', 0);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->group_by('order_items.ProductID');
        $this->db->order_by('OrderCount', $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return array();
        }
    }

    public function getSimilarProducts($where = false, $system_language_code = 'EN', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('products.*, products_text.*, COUNT(order_items.ProductID) as OrderCount, users.BoothUserName, users_text.BoothName');
        $this->db->from('order_items');
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('users', 'users.UserID = products.UserID', 'LEFT');
        $this->db->join('users_text', 'users_text.UserID = users.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->having('OrderCount >', 0);
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->group_by('order_items.ProductID');
        $this->db->order_by('OrderCount', $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return array();
        }
    }

}