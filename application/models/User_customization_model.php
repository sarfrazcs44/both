<?php

Class User_customization_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_customization");

    }

    public function getUserProfileCustomization($UserID)
    {
        $this->db->select('user_customization.*, themes.Image as ThemeImage, themes_text.Title as ThemeTitle, tst.StyleTitleEn as TopStyleTitleEn, tst.StyleTitleAr as TopStyleTitleAr, tst.StyleImage as TopStyleImage, tsb.StyleTitleEn as BottomStyleTitleEn, tsb.StyleTitleAr as BottomStyleTitleAr, tsb.StyleImage as BottomStyleImage');
        $this->db->from('user_customization');
        $this->db->join('themes', 'user_customization.ThemeID = themes.ThemeID', 'LEFT');
        $this->db->join('themes_text', 'themes.ThemeID = themes_text.ThemeID');
        $this->db->join('theme_styles tst', 'user_customization.TopStyleID = tst.ThemeStyleID');
        $this->db->join('theme_styles tsb', 'user_customization.BottomStyleID = tsb.ThemeStyleID');
        $this->db->where('user_customization.UserID', $UserID);
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->row_array();
        } else {
            return array();
        }
    }

}