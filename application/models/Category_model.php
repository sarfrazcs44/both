<?php

Class Category_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("categories");

    }

    public function getCategoriesWhereIn($user_categories_arr, $system_language_code = 'EN')
    {
        $this->db->select('categories.*, categories_text.*');
        $this->db->from('categories');
        $this->db->join('categories_text', 'categories.CategoryID = categories_text.CategoryID');
        $this->db->where_in('categories.CategoryID', $user_categories_arr);
        $this->db->join('system_languages', 'categories_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return array();
        }
    }

    public function getParentCategories($user_categories_arr)
    {
        $this->db->select('categories.ParentID');
        $this->db->from('categories');
        $this->db->join('categories_text', 'categories.CategoryID = categories_text.CategoryID AND categories_text.SystemLanguageID = 1');
        $this->db->where_in('categories.CategoryID', $user_categories_arr);
        $this->db->group_by('categories.ParentID');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

}