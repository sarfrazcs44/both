<?php

Class User_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("users");

    }

    public function getUserInfo($where, $system_language_code = 'EN')
    {
        if($system_language_code == 'EN'){
            $lang_id = 1;
        }else{
            $lang_id = 2;
        }
        $this->db->select('users.*,users_text.*,cities_text.Title as CityTitle, countries.Currency, countries.CurrencySymbol,packages_text.Title as PackageTitle,packages.PackagePrice,packages.Duration');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('cities', 'cities.CityID = users.CityID', 'Left');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID');
        $this->db->join('countries', 'cities.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('countries_text', 'countries.CountryID = countries_text.CountryID');

        $this->db->join('packages', 'packages.PackageID = users.PackageID', 'Left');
        $this->db->join('packages_text', 'packages_text.PackageID = packages.PackageID AND packages_text.SystemLanguageID = '.$lang_id.'', 'Left');

        $this->db->join('system_languages', 'system_languages.SystemLanguageID = cities_text.SystemLanguageID');
        $this->db->join('system_languages slpt', 'countries_text.SystemLanguageID = slpt.SystemLanguageID');
        
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('slpt.ShortCode', $system_language_code);
       
        $this->db->where($where);
        $result = $this->db->get();

        return $result->row_array();


    }

    public function getUsersForExpiryNotification($day = 30){
        $this->db->select('*');
        $this->db->from('users');
        $where = 'DATEDIFF(PackageExpiry,CURDATE()) = '.$day.'';
        $this->db->where($where);
        $this->db->where('RoleID',2);
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
        return $result->result_array();
    }

    public function getUserData($data, $system_language_code = false)
    {

        $this->db->select('users.*,users_text.*,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('roles', 'roles.RoleID = users.RoleID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
        // $this->db->where($where);
        $this->db->where('users.Email', $data['Email']);
        //$this->db->or_where('users.UName',$data['Email']);
        $this->db->where('users.Password', $data['Password']);

        /*if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }*/
        $this->db->where('system_languages.IsDefault', '1');
        return $this->db->get()->row_array();


    }

    public function getUsers($where = false, $system_language_code = 'EN', $sort_by = 'users.UserID', $sort_as = 'DESC', $limit = false, $start = 0,$category_join = false)
    {
        $this->db->select('users.*,users_text.*,cities_text.Title as CityTitle,roles_text.Title as RoleTitle, countries.Currency, countries.CurrencySymbol, countries_text.Title as CountryTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        if($category_join){
            $this->db->join('user_categories', 'users.UserID = user_categories.UserID');
            $this->db->join('categories', 'categories.CategoryID = user_categories.CategoryID','left');
            $this->db->join('categories_text', 'categories.CategoryID = categories_text.CategoryID','left');
        }
        
        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID AND roles_text.SystemLanguageID = 1');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID', 'LEFT');
        $this->db->join('countries', 'cities.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('countries_text', 'countries.CountryID = countries_text.CountryID');
        $this->db->join('system_languages', 'cities_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->join('system_languages slpt', 'countries_text.SystemLanguageID = slpt.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }

        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('slpt.ShortCode', $system_language_code);

        $this->db->group_by('users.UserID');
        $this->db->order_by($sort_by, $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();


    }

    public function getAdminUsers($where)
    {

        $this->db->select('users.*,users_text.*,roles_text.Title as RoleTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID AND roles_text.SystemLanguageID = 1');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1', 'LEFT');
        $this->db->where($where);
        $this->db->group_by('users.UserID');
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();

    }

    public function getBlockedUsers($UserID,$UserType = false)
    {
        $this->db->select('users_blocked.BlockedUserType, users_blocked.BlockedAt, users.*, users_text.*');
        $this->db->from('users_blocked');
        $this->db->join('users', 'users_blocked.BlockedUserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->where('users_blocked.UserID', $UserID);
        if($UserType){
            $this->db->where('users_blocked.UserType', $UserType);
        }
        
        return $this->db->get()->result_array();
    }

    public function getBoothRatings($where = false)
    {
        $this->db->select('orders_requests.OrderRequestRating,orders_requests.OrderRequestReview, users_text.*, users.*');
        $this->db->from('orders_requests');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->join('users', 'orders.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->where('orders_requests.OrderRequestRating >', '0');
        if ($where) {
            $this->db->where($where);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return array();
        }
    }


    public function getUserRatings($where = false)
    {
        $this->db->select('orders_requests.UserOrderRequestRating,orders_requests.UserOrderRequestReview, users_text.*, users.*');
        $this->db->from('orders_requests');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->join('users', 'orders_requests.BoothID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->where('orders_requests.UserOrderRequestRating >', '0');
        if ($where) {
            $this->db->where($where);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return array();
        }
    }

    public function getAverageRating($BoothID)
    {
        $this->db->select('AVG(orders_requests.OrderRequestRating) as average_rating');
        $this->db->from('orders_requests');
        $this->db->where('orders_requests.BoothID', $BoothID);
        // $this->db->where('orders_requests.OrderStatusID', 4);
        $this->db->where('orders_requests.OrderRequestRating >', '0');
        $result = $this->db->get();
        return $result->row();
    }

    public function getRatingsCount($BoothID)
    {
        $this->db->select('COUNT(orders_requests.OrderRequestID) as TotalRatings');
        $this->db->from('orders_requests');
        $this->db->where('BoothID', $BoothID);
        $this->db->where('orders_requests.OrderRequestRating >', '0');
        $result = $this->db->get();
        return $result->row();
    }

    public function getUserAverageRating($UserID)
    {
        $this->db->select('AVG(orders_requests.UserOrderRequestRating) as average_rating');
        $this->db->from('orders_requests');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->where('orders.UserID', $UserID);
        // $this->db->where('orders_requests.OrderStatusID', 4);
        $this->db->where('orders_requests.UserOrderRequestRating >', '0');
        $result = $this->db->get();
        return $result->row();
    }

    public function getUserRatingsCount($UserID)
    {
        $this->db->select('COUNT(orders_requests.OrderRequestID) as TotalRatings');
        $this->db->from('orders_requests');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->where('orders.UserID', $UserID);
        $this->db->where('orders_requests.UserOrderRequestRating >', '0');
        $result = $this->db->get();
        return $result->row();
    }

}

?>