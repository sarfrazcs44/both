<?php

Class Product_reported_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("product_reported");

    }

    public function getCountProductReports($ProductID)
    {
        $this->db->select('COUNT(ProductID) as Total');
        $this->db->from('product_reported');
        $this->db->where('ProductID', $ProductID);

        $result = $this->db->get();
        return $result->row();
    }

    public function getProductReports($ProductID)
    {
        $this->db->select('product_reported.*, users.UserID, users.CompressedImage, users_text.FullName');
        $this->db->from('product_reported');
        $this->db->join('users', 'product_reported.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->where('product_reported.ProductID', $ProductID);

        $result = $this->db->get();
        return $result->result();
    }

    public function getUserProductReports($UserID)
    {
        $this->db->select('product_reported.*, products.*, products_text.*');
        $this->db->from('product_reported');
        $this->db->join('products', 'product_reported.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = 1');
        $this->db->where('product_reported.UserID', $UserID);

        $result = $this->db->get();
        return $result->result();
    }


}