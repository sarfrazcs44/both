<?php

Class User_post_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_posts");
    }



    public function getPosts($where = false){

    	$this->db->select('user_posts.*,user_posts.UserPostID,user_posts.Image as PostImage,cities_text.title as CityTitle,users.UserName,users.BoothUserName,users.Email,users.CompressedImage,users.CompressedCoverImage,users.Image,users.BoothImage,users.CompressedBoothImage');
    	$this->db->from('user_posts');
    	$this->db->join('users','users.UserID = user_posts.UserID');
    	$this->db->join('cities_text','users.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1');
    	if($where){
    		$this->db->where($where);
    	}
    	
        $result = $this->db->get();
        return $result->result_array();
    }


}