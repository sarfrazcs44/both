<?php

Class Chat_message_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("chat_messages");
    }

    public function getChatMessagesDates($where = false)
    {
        $this->db->select("DATE_FORMAT(FROM_UNIXTIME(CreatedAt), '%Y-%m-%d') as MessageDate,chats.ChatID");
        $this->db->from('chats');
        $this->db->join('chat_messages', 'chat_messages.ChatID = chats.ChatID','left');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('MessageDate');
        // $this->db->order_by($sort_by, $sort_as);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getChatMessages($where = false, $start = 0, $limit = false, $sort_by = 'cm.ChatMessageID', $sort_as = 'DESC')
    {
       // $this->db->select('cm.*, su.UserID as SenderID, ru.UserID as ReceiverID, sut.FullName as SenderName, sut.BoothName as SenderBoothName, rut.FullName as ReceiverName, rut.BoothName as ReceiverBoothName, su.CompressedImage as SenderImage, su.CompressedBoothImage as SenderBoothImage, ru.CompressedImage as ReceiverImage, ru.CompressedBoothImage as ReceiverBoothImage');
         $this->db->select('cm.*, su.UserID as SenderID, ru.UserID as ReceiverID');
        $this->db->from('chat_messages cm');
        $this->db->join('chats c', 'cm.ChatID = c.ChatID');
        $this->db->join('users su', 'cm.SenderID = su.UserID');
        $this->db->join('users_text sut', 'su.UserID = sut.UserID AND sut.SystemLanguageID = 1');
        $this->db->join('users ru', 'cm.ReceiverID = ru.UserID');
        $this->db->join('users_text rut', 'ru.UserID = rut.UserID AND rut.SystemLanguageID = 1');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->order_by($sort_by, $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return array();
        }
    }

    public function getLastMessageForThisChatRoom($chatRoomID)
    {
        $this->db->select('cm.*, su.UserID as SenderUserID, ru.UserID as ReceiverUserID,su.UserName as SenderUserName,su.BoothUserName as SenderBoothUserName, sut.FullName as SenderName,sut.BoothName as SenderBoothName,ru.UserName as ReceiverUserName ,ru.BoothUserName as ReceiverBoothUserName , rut.FullName as ReceiverName, rut.BoothName as ReceiverBoothName, su.CompressedImage as SenderImage, su.CompressedBoothImage as SenderBoothImage, ru.CompressedImage as ReceiverImage, ru.CompressedBoothImage as ReceiverBoothImage');
        $this->db->from('chat_messages cm');
        $this->db->join('users su', 'cm.SenderID = su.UserID');
        $this->db->join('users ru', 'cm.ReceiverID = ru.UserID');
        $this->db->join('users_text sut', 'cm.SenderID = sut.UserID AND sut.SystemLanguageID = 1');
        $this->db->join('users_text rut', 'cm.ReceiverID = rut.UserID AND rut.SystemLanguageID = 1');
        $this->db->where('cm.ChatID', $chatRoomID);
        $this->db->order_by('cm.CreatedAt', 'DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return array();
        }

    }

}