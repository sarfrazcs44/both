<?php

Class Question_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct('questions');
    }

    public function getQuestions($where = false, $system_language_code = 'EN', $data_array = false, $type = 'categories', $limit = false, $start = 0)
    {
        $this->db->select('questions.*, users.CompressedImage as UserImage, users.UserName, users.BoothUserName, users_text.FullName, users_text.BoothName, cities_text.Title as UserCityName, sct.Title as SubCategoryName, ct.Title as CategoryName');

        $this->db->from('questions');

        $this->db->join('users', 'users.UserID = questions.UserID', 'LEFT');
        $this->db->join('users_text', 'users_text.UserID = users.UserID AND users_text.SystemLanguageID = 1');

        $this->db->join('cities', 'cities.CityID = users.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID');

        // sub category
        $this->db->join('categories sc', 'questions.CategoryID = sc.CategoryID', 'LEFT');
        $this->db->join('categories_text sct', 'sc.CategoryID = sct.CategoryID');

        // category
        $this->db->join('categories c', 'sc.ParentID = c.CategoryID', 'LEFT');
        $this->db->join('categories_text ct', 'c.CategoryID = ct.CategoryID');

        // user city join with system_languages
        $this->db->join('system_languages sluct', 'cities_text.SystemLanguageID = sluct.SystemLanguageID');

        // sub category join with system_languages
        $this->db->join('system_languages slsct', 'sct.SystemLanguageID = slsct.SystemLanguageID');

        // category join with system_languages
        $this->db->join('system_languages slct', 'ct.SystemLanguageID = slct.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }

        if ($data_array) {
            if ($type === 'categories') {
                $this->db->where_in('sc.CategoryID', $data_array);
            } elseif ($type === 'booths') {
                $this->db->where_in('questions.BoothID', $data_array);
            }
        }

        $this->db->where('sluct.ShortCode', $system_language_code);

        $this->db->where('slsct.ShortCode', $system_language_code);
        $this->db->where('slct.ShortCode', $system_language_code);

        $this->db->group_by('questions.QuestionAskedAt');
        $this->db->order_by('questions.QuestionAskedAt', 'DESC');
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }


    public function getQuestionDetail($where = false)
    {
        $this->db->select('questions.*');
        $this->db->from('questions');
        
        

        $this->db->join('users','users.UserID = questions.UserID','left');

        if($where){
             $this->db->where($where);
        }

        
        $result = $this->db->get();
        return $result->row();
    }

}