<?php

Class User_post_reported_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_posts_reported");

    }

    public function getCountReports($UserPostID)
    {
        $this->db->select('COUNT(UserPostID) as Total');
        $this->db->from('user_posts_reported');
        $this->db->where('UserPostID', $UserPostID);

        $result = $this->db->get();
        return $result->row();
    }

    public function getReports($UserPostID)
    {
        $this->db->select('user_posts_reported.*, users.UserID, users.CompressedImage, users_text.FullName, users_text.BoothName, users.UserName, users.BoothUserName');
        $this->db->from('user_posts_reported');
        $this->db->join('users', 'user_posts_reported.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->where('user_posts_reported.UserPostID', $UserPostID);

        $result = $this->db->get();
        return $result->result();
    }


}