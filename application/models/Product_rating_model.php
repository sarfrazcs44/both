<?php

Class Product_rating_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("product_ratings");
    }

    public function getProductRatings($where = false)
    {
        $this->db->select('product_ratings.*, users_text.*, users.*');
        $this->db->from('product_ratings');
        $this->db->join('users', 'product_ratings.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        if ($where) {
            $this->db->where($where);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return array();
        }
    }

    public function getAverageRating($ProductID)
    {
        $this->db->select('AVG(Rating) as average_rating');
        $this->db->from('product_ratings');
        $this->db->where('ProductID', $ProductID);
        $result = $this->db->get();
        return $result->row();
    }

    public function getRatingsCount($ProductID)
    {
        $this->db->select('COUNT(product_ratings.ProductRatingID) as TotalRatings');
        $this->db->from('product_ratings');
        $this->db->where('ProductID', $ProductID);
        $result = $this->db->get();
        return $result->row();
    }

}