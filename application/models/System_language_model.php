<?php
Class System_language_model extends Base_Model
{
    
   
    public function __construct()
    {
        parent::__construct("system_languages");
        
    }
    
    
    
    
    public function getAllLanguages(){
        
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('Hide',0);
        $this->db->order_by('IsDefault','DESC');
        
        return $this->db->get()->result();
        
    } 
    
}

    