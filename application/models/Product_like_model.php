<?php

Class Product_like_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("product_likes");

    }

    public function getProductLikesWhere($where = false, $system_language_code = 'EN')
    {
        $this->db->select('product_likes.*, users.UserID, users.CompressedImage, users_text.FullName');
        $this->db->from('product_likes');
        $this->db->join('users', 'product_likes.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        if ($where) {
            $this->db->where($where);
        }

        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return array();
        }
    }

    public function getCountProductLikes($ProductID)
    {
        $this->db->select('COUNT(ProductID) as Total');
        $this->db->from('product_likes');
        $this->db->where('ProductID', $ProductID);

        $result = $this->db->get();
        return $result->row();
    }

    public function getProductLikes($ProductID, $system_language_code = 'EN')
    {
        $this->db->select('product_likes.*, users.UserID, users.CompressedImage, users_text.FullName');
        $this->db->from('product_likes');
        $this->db->join('users', 'product_likes.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->where('product_likes.ProductID', $ProductID);

        $result = $this->db->get();
        return $result->result();
    }

    public function getUserLikedProducts($UserID, $system_language_code = 'EN')
    {
        $this->db->select('product_likes.*, products.ProductID, products_text.Title, users.UserID as BoothID, users_text.BoothName');
        $this->db->from('product_likes');
        $this->db->join('products', 'product_likes.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('users', 'products.UserID = users.UserID AND users.PackageExpiry >= CURDATE()');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('product_likes.UserID', $UserID);
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return array();
        }
    }

    public function getMostLikedProducts($system_language_code = 'EN', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('products.*, products_text.*, COUNT(product_likes.ProductID) as LikeCount');
        $this->db->from('product_likes');
        $this->db->join('products', 'product_likes.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->having('LikeCount >', 0);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->group_by('product_likes.ProductID');
        $this->db->order_by('LikeCount', $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return array();
        }
    }


}