<?php

Class Invite_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("invites");

    }

    public function getInvitees($where = false)
    {
        $this->db->select('*');
        $this->db->from('invites');
        if ($where) {
            $this->db->where($where);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }

}

?>