<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/jquery.tablednd.js"></script>
<?php
$MoreChild = true;
if ($ParentID > 0) {
    $category_detail = categoryDetail($ParentID);
    if (isset($category_detail) && $category_detail && $category_detail->ParentID > 0) {
        $MoreChild = false; // means its 3rd level
    }
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo($ParentID == 0 ? lang($ControllerName . 's') : lang('child_catagories_of') . ' ' . $parentData[0]->Title); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo($ParentID == 0 ? base_url('cms/' . $ControllerName . '/add') : base_url('cms/' . $ControllerName . '/add/' . $ParentID)); ?>">
                                <button type="button"
                                        class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo($ParentID == 0 ? lang('add_' . $ControllerName) : lang('add_child_' . $ControllerName)); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover sortable_tbl"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th><?php echo lang('title'); ?></th>
                                    <th>Sort Order</th>
                                    <th><?php echo lang('is_active'); ?></th>
                                    <?php if (checkUserRightAccess(43, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(43, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                // echo getCategoryTree($ParentID,'',$language);
                                if ($results) {
                                    foreach ($results as $value) { ?>
                                        <tr id="<?php echo $value->CategoryID; ?>">
                                            <td>
                                                <a href="<?php echo base_url('cms/category/index/' . $value->CategoryID); ?>"><?php echo $value->Title; ?></a>
                                            </td>
                                            <td><?php echo $value->SortOrder; ?></td>
                                            <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                            <?php if (checkUserRightAccess(43, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(43, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php if (checkUserRightAccess(43, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <?php
                                                        if ($MoreChild) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/index/' . $value->CategoryID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons">vertical_split</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php }
                                                        ?>
                                                        <a href="<?php echo base_url('cms/' . $ControllerName . '/edit/' . $value->CategoryID); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons">edit</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>

                                                    <?php if (checkUserRightAccess(43, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                        <a href="javascript:void(0);"
                                                           onclick="deleteRecord('<?php echo $value->CategoryID; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                           class="btn btn-simple btn-danger btn-icon remove"><i
                                                                    class="material-icons">close</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function() {
        $(".sortable_tbl").tableDnD({
            onDragClass: "myDragClass",
            onDrop: function(table, row) {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
                var StoreID = row.id;
                var rows = table.tBodies[0].rows;
                var IDsStr = '';
                for (var i=0; i<rows.length; i++) {
                    IDsStr += rows[i].id+",";
                }
                $.ajax({
                    type: "POST",
                    url: base_url + '' + 'cms/category/updateSorting',
                    data: {
                        'IDsStr': IDsStr,
                        'ParentID': <?php echo $ParentID; ?>
                    },
                    dataType: "json",
                    cache: false,
                    success: function (result) {
                        showSuccess(result.success);
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });
    });
</script>