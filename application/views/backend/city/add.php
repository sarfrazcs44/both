<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add') . ' ' . lang($ControllerName); ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label"
                                               for="pac-input"><?php echo lang('title'); ?></label>
                                        <input type="text" class="form-control" id="pac-input">
                                    </div>
                                </div>

                                <input type="hidden" name="CountryID" id="CountryID"
                                       value="<?php echo $country->CountryID; ?>" required>
                                <input type="hidden" name="Title" id="Title" required>
                                <input type="hidden" name="CityPlaceID" id="CityPlaceID" required>
                                <input type="hidden" name="CityLat" id="CityLat" required>
                                <input type="hidden" name="CityLong" id="CityLong" required>

                                <div class="col-sm-6 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"
                                                       checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="form-group label-floating text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light submit_btn" type="submit"
                                        disabled>
                                    <?php echo lang('submit'); ?>
                                </button>
                                <button type="button" class="btn btn-default waves-effect m-l-5"
                                        onclick="window.history.back();">
                                    <?php echo lang('back'); ?>
                                </button>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<script>
    $(document).ready(function () {
        // var map = new google.maps.Map(document.getElementById('map'), {
        //     center: {lat: 50.064192, lng: -130.605469},
        //     zoom: 3
        // });
        //var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        //var countries = document.getElementById('country-selector');

        // map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input, {types: ['(cities)'], language: ['(ar)']});

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        // autocomplete.bindTo('bounds', map);

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions({'country': ['<?php echo $country->CountryShortName; ?>']});

        // Set the data fields to return when the user selects a place.
        /*autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']
        );*/

        autocomplete.addListener('place_changed', function () {
            // showing loader
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });

            var place = autocomplete.getPlace();
            var city_name = place.name;
            var city_place_id = place.place_id;
            var city_lat = place.geometry.location.lat();
            var city_lng = place.geometry.location.lng();
            console.log(place);
            $('#pac-input').val(city_name);
            $('#Title').val(city_name);
            $('#CityPlaceID').val(city_place_id);
            $('#CityLat').val(city_lat);
            $('#CityLong').val(city_lng);
            $('.submit_btn').attr('disabled', false);
        });
    });
</script>