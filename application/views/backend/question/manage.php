<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Questions</h4>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>City</th>
                                    <th>Category</th>
                                    <th>Question Description</th>
                                    <th>Question Asked At</th>
                                    <th>Report Count</th>
                                    <?php if (checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($results) {
                                    foreach ($results as $value) {
                                        $value = (object)$value;
                                        ?>
                                        <tr id="<?php echo $value->QuestionID; ?>">
                                            <td><?php echo $value->FullName; ?></td>
                                            <td><?php echo $value->UserName; ?></td>
                                            <td><?php echo $value->UserCityName; ?></td>
                                            <td><?php echo $value->CategoryName; ?> / <?php echo $value->SubCategoryName; ?></td>
                                            <td><?php echo text_shorter($value->QuestionDescription); ?></td>
                                            <td><?php echo getFormattedDateTime($value->QuestionAskedAt, 'd-m-Y h:i:s A'); ?></td>
                                            <td><?php echo questionReportsCount($value->QuestionID); ?></td>
                                            <?php if (checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php if (checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <a href="<?php echo base_url('cms/questions/view/' . $value->QuestionID); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                class="material-icons"
                                                                title="Click to view details">visibility</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>

                                                    <?php if (checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->QuestionID; ?>','cms/questions/delete','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>