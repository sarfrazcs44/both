<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Edit Cancellation Reason</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/cancellationReason/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="OrderCancellationReasonID" value="<?php echo $result->OrderCancellationReasonID; ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CancellationReasonEn">Cancellation Reason En</label>
                                        <input type="text" name="CancellationReasonEn" required  class="form-control" id="CancellationReasonEn" value="<?php echo $result->CancellationReasonEn; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CancellationReasonAr">Cancellation Reason Ar</label>
                                        <input type="text" name="CancellationReasonAr" required  class="form-control" id="CancellationReasonAr" value="<?php echo $result->CancellationReasonAr; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group label-floating">
                                                <label class="control-label" for="Type">Reason For</label>
                                                <select id="Type" class="selectpicker" data-style="select-with-transition" required name="Type">
                                                    <option value="Buyer" <?php echo ($result->Type == 'Buyer' ? 'selected' : ''); ?>> Buyer</option>
                                                    <option value="Seller" <?php echo ($result->Type == 'Seller' ? 'selected' : ''); ?>> Seller</option>
                                                    
                                                </select>
                                        </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" <?php echo ($result->IsActive == 1 ? 'checked' : ''); ?>/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/cancellationReason">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>