<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        $common_fields5 = '';
        $common_fields6 = '';
        $category_dropdown = '';
        $sub_category_dropdown = '';
        $nutrition_dropdown = '';
        $boxes_dropdown = '';

        if ($key == 0) {
            foreach ($categories as $category) {
                $category_dropdown .= '<option ' . ($category->CategoryID == $result[$key]->CategoryID ? 'selected="selected"' : '') . ' value="' . $category->CategoryID . '">' . $category->Title . '</option>';
            }



            $common_fields4 = '<div class="row">';
            $product_images = get_images($result[$key]->ProductID, 'product');
            // print_rm($product_images);
            if ($product_images) {
                foreach ($product_images as $product_image) {
                    $common_fields4 .= '<div class="col-md-3 col-sm-3 col-xs-3"><i class="fa fa-trash delete_image" data-image-id="' . $product_image->SiteImageID . '" aria-hidden="true"></i><img src="' . base_url() . $product_image->ImageName . '" style="height:200px;width:200px;"></div>';
                }
            }
            $common_fields4 .= '<div class="col-md-12 col-sm-12 col-xs-12">

                                                <div class="form-group">

                                                    <label>Image</label>
                                                    <input type="file" name="Image[]" multiple="multiple">
                                                    <p>' . lang('cannot_upload_more_then') . '</p>
                                                </div>
                                            </div>
                                        </div>';


            $common_fields = '<div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="ProductPrice' . $key . '">' . lang('price') . '</label>
                                     <input type="text" name="ProductPrice" parsley-trigger="change" required  class="form-control number-with-decimals" id="ProductPrice" value="' . ((isset($result[$key]->ProductPrice)) ? $result[$key]->ProductPrice : '') . '">
                                                               
                                </div>
                          </div>';
            $common_fields2 = '<div class="row"><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsFeatured">
                                                <input name="IsFeatured" value="1" type="checkbox" id="IsFeatured" ' . ((isset($result[$key]->IsFeatured) && $result[$key]->IsFeatured == 1) ? 'checked' : '') . '/> ' . lang('is_featured') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="OutOfStock">
                                                <input name="OutOfStock" value="1" type="checkbox" id="OutOfStock" ' . ((isset($result[$key]->OutOfStock) && $result[$key]->OutOfStock == 1) ? 'checked' : '') . '/> ' . lang('out_of_stock') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsCustomizedProduct">
                                                <input name="IsCustomizedProduct" value="1" type="checkbox" id="IsCustomizedProduct" ' . ((isset($result[$key]->IsCustomizedProduct) && $result[$key]->IsCustomizedProduct == 1) ? 'checked' : '') . '/> ' . lang('is_customize') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </div>';

            $common_fields3 = '<div class="row">
                                    
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CategoryID" class="form-control" id="CategoryID" required>
                                            <option value="">' . lang('choose_category') . '</option>
                                            ' . $category_dropdown . '
                                        </select>
                                    </div>
                                </div>
                                
                                </div>
                            </div>';


        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                        ' . $common_fields . '
                                                         
                                                    </div>
                                                    ' . $common_fields3 . '


                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description">' . lang('description') . '</label>
                                                                <textarea class="form-control" name="Description" id="Description" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    



                                                    ' . $common_fields2 . '
                                                    ' . $common_fields4 . '
                                                   

                                                    
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>