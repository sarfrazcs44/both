<?php
if ($product->OutOfStock == 0) {
    $out_of_stock_msg = 'Mark as out of stock';
    $out_of_stock_cls = "btn btn-danger btn-sm";
    $out_of_stock_val = 1;
} elseif ($product->OutOfStock == 1) {
    $out_of_stock_msg = 'Mark as in stock';
    $out_of_stock_cls = "btn btn-success btn-sm";
    $out_of_stock_val = 0;
}
if ($product->IsActive == 0) {
    $is_active_msg = 'Mark as active';
    $is_active_cls = "btn btn-success btn-sm";
    $is_active_val = 1;
} elseif ($product->IsActive == 1) {
    $is_active_msg = 'Mark as inactive';
    $is_active_cls = "btn btn-danger btn-sm";
    $is_active_val = 0;
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="row">
                                <div class="col-sm-4">
                                    Product Details
                                </div>
                                <div class="col-sm-8 pull-right">
                                    <button class="<?php echo $out_of_stock_cls; ?>"
                                            onclick="ProductOutOfStock('<?php echo $product->ProductID; ?>', '<?php echo $out_of_stock_val; ?>');">
                                        <?php echo $out_of_stock_msg; ?>
                                        <div class="ripple-container"></div>
                                    </button>
                                    <button class="<?php echo $is_active_cls; ?>"
                                            onclick="ProductActive('<?php echo $product->ProductID; ?>', '<?php echo $is_active_val; ?>');">
                                        <?php echo $is_active_msg; ?>
                                        <div class="ripple-container"></div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Product Name</label>
                                        <h5><?php echo $product->Title; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Product Category</label>
                                        <h5><?php echo $product->CategoryName; ?>
                                            / <?php echo $product->SubCategoryName; ?>
                                            / <?php echo $product->SubSubCategoryName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Currency</label>
                                        <h5><?php echo $product->Currency; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Product Price</label>
                                        <h5><?php echo $product->ProductPrice; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Delivery Charges</label>
                                        <h5><?php echo $product->DeliveryCharges; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Est. Delivery Time</label>
                                        <h5><?php echo $product->DeliveryTime; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Product Type</label>
                                        <h5><?php echo $product->ProductType; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Out Of Stock?</label>
                                        <h5><?php echo $product->OutOfStock == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Active?</label>
                                        <h5><?php echo $product->IsActive == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Views Count</label>
                                        <h5><?php echo $product->ViewsCount; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Product Description</label>
                                        <h5><?php echo $product->ProductDescription; ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            Booth Details
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Booth Name</label>
                                        <h5>
                                            <a href="<?php echo base_url('cms/user/view/' . base64_encode($product->UserID)); ?>"
                                               target="_blank"><?php echo $product->BoothName; ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Booth Username</label>
                                        <h5><?php echo $product->BoothUserName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Booth City</label>
                                        <h5><?php echo $product->CityName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Booth Type</label>
                                        <h5><?php echo $product->BoothType; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Booth Verification</label>
                                        <h5><?php echo $product->Verification != '' ? $product->Verification : 'N/A'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Payment Account #</label>
                                        <h5><?php echo mask_number($product->PaymentAccountNumber); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Payment Account Title</label>
                                        <h5><?php echo $product->PaymentAccountHolderName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Payment Account Bank Branch</label>
                                        <h5><?php echo $product->PaymentAccountBankBranch; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Contact Days</label>
                                        <h5><?php echo $product->ContactDays; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Contact Timings</label>
                                        <h5><?php echo getFormattedDateTime($product->ContactTimeFrom, 'h:i A'); ?>
                                            - <?php echo getFormattedDateTime($product->ContactTimeTo, 'h:i A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            Product Images
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <?php
                            foreach ($product->ProductImages as $productImage) { ?>
                                <div class="col-sm-4" style="width: 200px;height: 200px;">
                                    <a data-fancybox="gallery"
                                       href="<?php echo base_url($productImage->ProductImage); ?>">
                                        <img src="<?php echo base_url($productImage->ProductCompressedImage); ?>">
                                    </a>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Product Likes (<?php echo $product->LikesCount; ?>)</h5>
                    </div>
                    <div class="card-content">
                        <?php
                        if (!empty($product->ProductLikes)) {
                            foreach ($product->ProductLikes as $productLike) { ?>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <a data-fancybox="gallery"
                                           href="<?php echo base_url($productLike->CompressedImage); ?>">
                                            <img src="<?php echo base_url($productLike->CompressedImage); ?>"
                                                 style="width: 40px;height: 40px;">
                                        </a>
                                    </div>
                                    <div class="col-sm-10"><a
                                                href="<?php echo base_url('cms/user/view/' . base64_encode($productLike->UserID)); ?>"
                                                target="_blank"><?php echo $productLike->FullName; ?></a></div>
                                </div>
                                <br>
                            <?php }
                        } else { ?>
                            <div class="row">
                                <div class="col-sm-12">No Likes Found</div>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Product Comments (<?php echo $product->CommentCount; ?>)</h5>
                    </div>
                    <div class="card-content">
                        <?php
                        if (!empty($product->ProductComments)) {
                            foreach ($product->ProductComments as $productComment) { ?>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <a data-fancybox="gallery"
                                           data-caption="<?php echo $productComment->FullName; ?>: <?php echo $productComment->Comment; ?>"
                                           href="<?php echo base_url($productComment->CompressedImage); ?>">
                                            <img src="<?php echo base_url($productComment->CompressedImage); ?>"
                                                 style="width: 40px;height: 40px;">
                                        </a>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="row">
                                            <div class="col-sm-12"><a
                                                        href="<?php echo base_url('cms/user/view/' . base64_encode($productComment->UserID)); ?>"
                                                        target="_blank"><?php echo $productComment->FullName; ?></a>
                                            </div>
                                            <div class="col-sm-12">
                                                <strong><?php echo $productComment->Comment; ?></strong></div>
                                            <div class="col-sm-12"><?php echo getFormattedDateTime($productComment->CreatedAt, 'd-m-Y h:i A'); ?></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            <?php }
                        } else { ?>
                            <div class="row">
                                <div class="col-sm-12">No Comments Found</div>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Product Reports (<?php echo $product->ReportCount; ?>)</h5>
                    </div>
                    <div class="card-content">
                        <?php
                        if (!empty($product->ProductReports)) {
                            foreach ($product->ProductReports as $productReport) { ?>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <a data-fancybox="gallery"
                                           data-caption="<?php echo $productReport->FullName; ?>: <?php echo $productReport->ReportReason; ?>"
                                           href="<?php echo base_url($productReport->CompressedImage); ?>">
                                            <img src="<?php echo base_url($productReport->CompressedImage); ?>"
                                                 style="width: 40px;height: 40px;">
                                        </a>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="row">
                                            <div class="col-sm-12"><a
                                                        href="<?php echo base_url('cms/user/view/' . base64_encode($productReport->UserID)); ?>"
                                                        target="_blank"><?php echo $productReport->FullName; ?></a>
                                            </div>
                                            <div class="col-sm-12">
                                                <strong><?php echo $productReport->ReportReason; ?></strong></div>
                                            <div class="col-sm-12"><?php echo getFormattedDateTime($productReport->ReportedAt, 'd-m-Y h:i A'); ?></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            <?php }
                        } else { ?>
                            <div class="row">
                                <div class="col-sm-12">No Reports Found</div>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.fancybox').fancybox({
            beforeShow: function () {
                this.title = $(this.element).data("caption");
            }
        });
    }); // ready
</script>