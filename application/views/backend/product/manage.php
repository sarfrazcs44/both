<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName . 's'); ?></h4>
                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#products" data-toggle="tab" aria-expanded="true">All Products</a>
                            </li>
                            <li class="">
                                <a href="#promotion_requests_approved" data-toggle="tab" aria-expanded="false">Promoted
                                    Products - Approved</a>
                            </li>
                            <li class="">
                                <a href="#promotion_requests_pending" data-toggle="tab" aria-expanded="false">Promoted
                                    Products - Pending</a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="products">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th><?php echo lang('title'); ?></th>
                                        <th><?php echo lang('category'); ?></th>
                                        <th><?php echo lang('booth_name'); ?></th>
                                        <th><?php echo lang('price'); ?></th>
                                        <th><?php echo lang('product_type'); ?></th>
                                        <th><?php echo lang('out_of_stock'); ?></th>
                                        <th><?php echo lang('is_active'); ?></th>
                                        <th>Report Count</th>
                                        <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($results) {
                                        foreach ($results as $value) {
                                            if ($value->OutOfStock == 0) {
                                                $out_of_stock_msg = 'Mark as out of stock';
                                                $out_of_stock_cls = "btn btn-danger btn-sm";
                                                $out_of_stock_val = 1;
                                            } elseif ($value->OutOfStock == 1) {
                                                $out_of_stock_msg = 'Mark as in stock';
                                                $out_of_stock_cls = "btn btn-success btn-sm";
                                                $out_of_stock_val = 0;
                                            }

                                            if ($value->IsActive == 0) {
                                                $is_active_msg = 'Mark as active';
                                                $is_active_cls = "btn-success";
                                                $is_active_val = 1;
                                                $is_active_icon = 'done';
                                            } elseif ($value->IsActive == 1) {
                                                $is_active_msg = 'Mark as inactive';
                                                $is_active_cls = "btn-danger";
                                                $is_active_val = 0;
                                                $is_active_icon = 'close';
                                            }
                                            ?>
                                            <tr id="<?php echo $value->ProductID; ?>">

                                                <td><?php echo $value->Title; ?></td>
                                                <td><?php echo $value->CategoryName; ?></td>
                                                <td><?php echo $value->BoothName.' (@'.$value->UserName.')'; ?></td>
                                                <td><?php echo $value->ProductPrice . ' ' . $value->Currency; ?></td>
                                                <td><?php echo $value->ProductType; ?></td>

                                                <td><?php echo($value->OutOfStock == 1 ? lang('yes') : lang('no')); ?></td>
                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                                <td><?php echo productReportsCount($value->ProductID); ?></td>

                                                <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td>
                                                        <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . $value->ProductID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons"
                                                                        title="Click to view details">visibility</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <a href="javascript:void(0);"
                                                               onclick="ProductOutOfStock('<?php echo $value->ProductID; ?>', '<?php echo $out_of_stock_val; ?>');"
                                                               class="btn btn-simple <?php echo $out_of_stock_cls; ?> btn-icon remove"><i
                                                                        class="material-icons"
                                                                        title="<?php echo $out_of_stock_msg; ?>">store</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>

                                                        <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                            <!--<a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $value->ProductID;*/ ?>','cms/<?php /*echo $ControllerName; */ ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>-->
                                                            <a href="javascript:void(0);"
                                                               onclick="ProductActive('<?php echo $value->ProductID; ?>', '<?php echo $is_active_val; ?>');"
                                                               class="btn btn-simple <?php echo $is_active_cls; ?> btn-icon remove"><i
                                                                        class="material-icons"
                                                                        title="<?php echo $is_active_msg; ?>"><?php echo $is_active_icon; ?></i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="promotion_requests_approved">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th><?php echo lang('title'); ?></th>
                                        <th><?php echo lang('category'); ?></th>
                                        <th><?php echo lang('booth_name'); ?></th>
                                        <th><?php echo lang('price'); ?></th>
                                        <th><?php echo lang('product_type'); ?></th>
                                        <th><?php echo lang('out_of_stock'); ?></th>
                                        <th><?php echo lang('is_active'); ?></th>
                                        <th>Report Count</th>
                                        <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($results_approved) {
                                        foreach ($results_approved as $value) {?>
                                            <tr id="<?php echo $value->ProductID; ?>">
                                                <td><?php echo $value->Title; ?></td>
                                                <td><?php echo $value->CategoryName; ?></td>
                                                <td><?php echo $value->BoothName.' (@'.$value->UserName.')'; ?></td>
                                                <td><?php echo $value->ProductPrice . ' ' . $value->Currency; ?></td>
                                                <td><?php echo $value->ProductType; ?></td>
                                                <td><?php echo($value->OutOfStock == 1 ? lang('yes') : lang('no')); ?></td>
                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                                <td><?php echo productReportsCount($value->ProductID); ?></td>
                                                <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td>
                                                        <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . $value->ProductID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons"
                                                                        title="Click to view details">visibility</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="promotion_requests_pending">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th><?php echo lang('title'); ?></th>
                                        <th><?php echo lang('category'); ?></th>
                                        <th><?php echo lang('booth_name'); ?></th>
                                        <th><?php echo lang('price'); ?></th>
                                        <th><?php echo lang('product_type'); ?></th>
                                        <th><?php echo lang('out_of_stock'); ?></th>
                                        <th><?php echo lang('is_active'); ?></th>
                                        <th>Report Count</th>
                                        <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($results_pending) {
                                        foreach ($results_pending as $value) {?>
                                            <tr id="<?php echo $value->ProductID; ?>">
                                                <td><?php echo $value->Title; ?></td>
                                                <td><?php echo $value->CategoryName; ?></td>
                                                <td><?php echo $value->BoothName.' (@'.$value->UserName.')'; ?></td>
                                                <td><?php echo $value->ProductPrice . ' ' . $value->Currency; ?></td>
                                                <td><?php echo $value->ProductType; ?></td>
                                                <td><?php echo($value->OutOfStock == 1 ? lang('yes') : lang('no')); ?></td>
                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                                <td><?php echo productReportsCount($value->ProductID); ?></td>
                                                <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td>
                                                        <?php if (checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . $value->ProductID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons"
                                                                        title="Click to view details">visibility</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <a href="javascript:void(0);"
                                                               onclick="ApprovePromotion('<?php echo $value->ProductID; ?>');"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons"
                                                                        title="Click to approve">thumb_up</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>