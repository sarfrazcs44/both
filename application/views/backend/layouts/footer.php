<footer class="footer">
    <audio id="audiotag" src="<?php echo base_url('assets/bell.mp3'); ?>" preload="auto"></audio>
    <div class="container-fluid">
        <p class="copyright pull-right">
           
                    Copyrights | Booth-in  <?php echo date('Y'); ?>
        </p>
    </div>
</footer>
</div>
</div>
</body>


<!-- Forms Validations Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo base_url(); ?>assets/backend/js/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.21/moment-timezone-with-data.js"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-notify.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<!--   Sharrre Library    -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!-- Select Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.select-bootstrap.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<!--  DataTables.net Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?php echo base_url(); ?>assets/backend/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/summernote/summernote.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url(); ?>assets/backend/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url(); ?>assets/backend/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/script.js?v=<?php echo rand(); ?>"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.blockUI.js"></script>
<!--
<script src="<?php echo base_url(); ?>assets/backend/plugins/jquery.filer/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.fileuploads.init.js"></script>-->

<?php if ($this->session->flashdata('message')) { ?>
    <script>

        $(document).ready(function () {
            showError('<?php echo $this->session->flashdata('message'); ?>');
        });

    </script>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function () {
        /*$('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });*/

        $(".summernote").summernote({
            styleWithSpan: true,
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });

        $('.inline-editor').summernote({
            airMode: true
        });

        $('.custom_datepicker').datetimepicker({format: 'DD-MM-YYYY'});
    });

    setTimeout(function () {
        $(".summernote-arb").css("direction", "rtl");
    }, 1000);
</script>

<script>
    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    function eraseCookie(name) {
        document.cookie = name + '=; Max-Age=-99999999;';
    }

    var base_url = "<?php echo base_url() ?>";
    var system_timezone = moment.tz.guess();
    eraseCookie('system_timezone');
    setCookie('system_timezone', system_timezone, 7);
    console.log("Local system timezone is: " + system_timezone);
</script>

<script type="text/javascript">
    $('.my_timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 15,
        minTime: '4',
        maxTime: '11:00pm',
        startTime: '4:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
</script>

<script>
    $(document).ready(function () {
        // Basic instantiation:
        $('#Color').colorpicker();

        // Example using an event, to change the color of the .jumbotron background:
        $('#Color').on('colorpickerChange', function(event) {
            // $('.jumbotron').css('background-color', event.color.toString());
            $('#Color').val(event.color.toString());
        });
    });
    function playAudio() {
        var audiotag = document.getElementById('audiotag');
        audiotag.play();
    }
</script>

<script>

    // Enable pusher logging - don't include this in production
    /*Pusher.logToConsole = true;

    var pusher = new Pusher('3545e504b911a95dee5f', {
        cluster: 'ap2',
        forceTLS: true
    });

    var channel = pusher.subscribe('MPKSA_Channel');
    channel.bind('MPKSA_Event', function(data) {
        console.log(JSON.stringify(data));
        playAudio();
        setTimeout(function () {
            window.location.reload();
        }, 2000);
    });*/
</script>

<script>
    $(document).ready(function () {
        $('table.datatable').DataTable({
            "ordering": true/*,
            "aaSorting": []*/
        });
    });
</script>

</html>
