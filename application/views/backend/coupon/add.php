<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Add Promo code</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required class="form-control" id="Title">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CouponCode">Promo Code</label>
                                        <input type="text" name="CouponCode" required class="form-control"
                                               id="CouponCode">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary"
                                       onclick="generateCouponCode();">Generate</a>
                                </div>
                                <!--<div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="UsageCount">Available Usage Count</label>
                                        <input type="number" name="UsageCount" required class="form-control"
                                               id="UsageCount">
                                    </div>
                                </div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DiscountType">Discount Type</label>
                                        <select class="selectpicker" id="DiscountType"
                                                data-style="select-with-transition" required name="DiscountType">
                                            <option value="Fixed">Fixed</option>
                                            <option value="Percentage">Percentage</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DiscountFactor">Discount Factor</label>
                                        <input type="number" name="DiscountFactor" required
                                               class="form-control maxValue number-with-decimals"
                                               id="DiscountFactor">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ExpiryDate">Expiry Date</label>
                                        <input type="text" name="ExpiryDate" required
                                               class="form-control custom_datepicker" id="ExpiryDate">
                                    </div>
                                </div>
                            </div>
                            <!--<div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CouponType">Coupon Type</label>
                                        <select class="selectpicker" id="CouponType"
                                                data-style="select-with-transition" required name="CouponType">
                                            <option value="Generic">Generic</option>
                                            <option value="Seller">Seller</option>
                                        </select>
                                    </div>
                                </div>
                            </div>-->
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"
                                                       checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit'); ?>
                                </button>
                                <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back'); ?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".maxsValue").attr({
            "max": 10,        // substitute your own
            "min": 2          // values (or variables) here
        });
    });

    function generateCouponCode() {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $form = $(this);
        $.ajax({
            type: "GET",
            url: base_url + "cms/coupon/generateCouponCode",
            success: function (result) {
                $('#CouponCode').closest('.form-group').removeClass('is-empty');
                $('#CouponCode').closest('.form-group').removeClass('has-error');
                $('#CouponCode').closest('.form-group').addClass('is-focused');
                $('#CouponCode').val(result);
            },
            complete: function () {
                $.unblockUI();
            }
        });
    }
</script>