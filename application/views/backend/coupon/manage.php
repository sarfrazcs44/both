<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">

                        <h4 class="card-title">Promo codes</h4>
                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#admin_created" data-toggle="tab" aria-expanded="true">Admin Created</a>
                            </li>
                            <li class="">
                                <a href="#booth_created" data-toggle="tab" aria-expanded="false">Booth Created</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="admin_created">
                                <div class="toolbar">
                                    <a href="<?php echo base_url('cms/' . $ControllerName . '/add'); ?>">
                                        <button type="button"
                                                class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Promo Code
                                        </button>
                                    </a>
                                </div>
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th><?php echo lang('title'); ?></th>
                                        <th>Promo Code</th>
                                        <th>Discount Type</th>
                                        <th>Discount Factor</th>
                                       <!-- <th>Available Usage Count</th>-->
                                        <th>Expiry Date</th>
                                        <th>Status</th>
                                        <th><?php echo lang('is_active'); ?></th>
                                        <?php if (checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($admin_created_coupons) {
                                        foreach ($admin_created_coupons as $value) {
                                            $today = date('d-m-Y');
                                            $todays_timestamp = strtotime($today);
                                            if ($value->ExpiryDate >= $todays_timestamp) {
                                                $style = "style=\"color: green; font-weight: bold;\"";
                                                $status = "Available";
                                            } else {
                                                $style = "style=\"color: red; font-weight: bold;\"";
                                                $status = "Expired";
                                            }

                                            if ($value->DiscountType == 'Percentage') {
                                                $factor = "%";
                                            } else {
                                                $factor = "SAR";
                                            }
                                            ?>
                                            <tr id="<?php echo $value->CouponID; ?>">
                                                <td><?php echo $value->Title; ?></td>
                                                <td><?php echo $value->CouponCode; ?></td>
                                                <td><?php echo $value->DiscountType; ?></td>
                                                <td><?php echo $value->DiscountFactor; ?><?php echo $factor; ?></td>
                                               <!-- <td><?php echo $value->UsageCount; ?></td>-->
                                                <td><?php echo $value->ExpiryDate != '' ? getFormattedDateTime($value->ExpiryDate, 'd-m-Y') : 'N/A'; ?></td>
                                                <td><span <?php echo $style; ?>><?php echo $status; ?></span></td>
                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                                <?php if (checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td>
                                                        <?php if (checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                            <a href="javascript:void(0);"
                                                               onclick="deleteRecord('<?php echo $value->CouponID; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="booth_created">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Booth Name</th>
                                        <th><?php echo lang('title'); ?></th>
                                        <th>Promo code</th>
                                        <th>Discount Type</th>
                                        <th>Discount Factor</th>
                                        <!--<th>Available Usage Count</th>-->
                                        <th>Expiry Date</th>
                                        <th>Status</th>
                                        <th><?php echo lang('is_active'); ?></th>
                                        <?php if (checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($booth_created_coupons) {
                                        foreach ($booth_created_coupons as $value) {
                                            $today = date('d-m-Y');
                                            $todays_timestamp = strtotime($today);
                                            if ($value->ExpiryDate >= $todays_timestamp) {
                                                $style = "style=\"color: green; font-weight: bold;\"";
                                                $status = "Available";
                                            } else {
                                                $style = "style=\"color: red; font-weight: bold;\"";
                                                $status = "Expired";
                                            }

                                            if ($value->DiscountType == 'Percentage') {
                                                $factor = "%";
                                            } else {
                                                $factor = "SAR";
                                            }
                                            $booth = getUserInfo($value->UserID, 'EN', true);
                                            ?>
                                            <tr id="<?php echo $value->CouponID; ?>">
                                                <td>
                                                    <a href="<?php echo base_url() . 'cms/user/view/' . base64_encode($value->UserID); ?>"
                                                       target="_blank"><?php echo $booth->BoothName; ?>
                                                        (@<?php echo $booth->BoothUserName; ?>)</a></td>
                                                <td><?php echo $value->Title; ?></td>
                                                <td><?php echo $value->CouponCode; ?></td>
                                                <td><?php echo $value->DiscountType; ?></td>
                                                <td><?php echo $value->DiscountFactor; ?><?php echo $factor; ?></td>
                                                <!--<td><?php echo $value->UsageCount; ?></td>-->
                                                <td><?php echo $value->ExpiryDate != '' ? getFormattedDateTime($value->ExpiryDate, 'd-m-Y') : 'N/A'; ?></td>
                                                <td><span <?php echo $style; ?>><?php echo $status; ?></span></td>
                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                                <?php if (checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td>
                                                        <?php if (checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                            <a href="javascript:void(0);"
                                                               onclick="deleteRecord('<?php echo $value->CouponID; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>