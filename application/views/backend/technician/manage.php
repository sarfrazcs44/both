<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Technicians</h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Technician</button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile No.</th>
                                    <th>City</th>
                                    <th>Company Name</th>
                                    <th>Online Status</th>
                                    <th>Image</th>
                                    <th><?php echo lang('is_active');?></th>
                                    <?php if(checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value->UserID;?>">
                                            <td><?php echo $value->FullName; ?></td>
                                            <td><?php echo $value->Email; ?></td>
                                            <td><?php echo $value->Mobile; ?></td>
                                            <td><?php echo $value->CityTitle; ?></td>
                                            <td><?php echo $value->CompanyName; ?></td>
                                            <td><?php echo $value->OnlineStatus; ?></td>
                                            <td>
                                                <a data-fancybox="gallery" class="fancybox" title="<?php echo $value->FullName; ?>" href="<?php echo base_url().'/'.$value->Image; ?>">
                                                    <img src="<?php echo base_url().'/'.$value->Image; ?>" style="height: 60px; width: 60px;">
                                                </a>
                                            </td>

                                            <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?> 
                                            <td>
                                                <?php if(checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->UserID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                               
                                                <?php if(checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->UserID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>