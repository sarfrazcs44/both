<!DOCTYPE html>
<html>
<head>
    <title>Details For Booking # <?php echo $booking['OrderNumber']; ?></title>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Poppins');

        body,
        table {
            font-family: 'Poppins', sans-serif;
        }
    </style>
</head>
<body>
<table style="width: 100%;margin: 2px auto;border-collapse: collapse;table-layout: fixed; font-size: 14px;">
    <tbody>
    <tr>
        <td colspan="4" style="text-align: center;padding: 15px 0;border: 1px solid #999;">
            <h4 style="margin: 0;">Details For Booking # <b><?php echo $booking['OrderNumber']; ?></b></h4>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Service Title</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['CategoryTitle']; ?></td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Service Date</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['BookingDate'] != '' ? getFormattedDateTime($booking['BookingDate'], 'l\, F jS\, Y') : 'N/A'; ?></td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Service Time</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['BookingTime'] != '' ? getFormattedDateTime($booking['BookingTime'], 'h:i A') : 'N/A'; ?></td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Booking Description</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo($booking['Description'] != '' ? custom_decode($booking['Description']) : 'N/A'); ?></td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Image</td>
        <td style="padding: 10px 16px;border: 1px solid #999;">
            <?php if ($booking['Image'] != '') { ?>
                <a href="<?php echo base_url() . $booking['Image']; ?>" target="_blank"
                   title="Click to view in new tab"><img
                            src="<?php echo base_url() . $booking['Image']; ?>"
                            style="width: 160px !important;height: 160px !important;"></a>
            <?php } else { ?>
                <h5>N/A</h5>
            <?php } ?>
        </td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Address</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo($booking['Address'] != '' ? custom_decode($booking['Address']) : 'N/A'); ?></td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center;padding: 15px 0;border: 1px solid #999;">
            <h4 style="margin: 0;">Customer Details</h4>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Customer Name / City</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['FullName']; ?>
            / <?php echo ucfirst($booking['UserCity']); ?>
        </td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Customer Mobile</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['Mobile']; ?></td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Customer Email</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['Email']; ?></td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Customer Type</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo ucfirst($booking['UserType']); ?></td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center;padding: 15px 0;border: 1px solid #999;">
            <h4 style="margin: 0;">Assigned Technician & Vehicle Details</h4>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Technician Name / City</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['TechnicianFullName']; ?>
            / <?php echo ucfirst($booking['TechnicianCity']); ?>
        </td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Technician Mobile</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['TechnicianMobile']; ?></td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Technician Email</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['TechnicianEmail']; ?></td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Vehicle Number / City</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['VehicleNumber']; ?>
            / <?php echo ucfirst($booking['VehicleCity']); ?>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center;padding: 15px 0;border: 1px solid #999;">
            <h4 style="margin: 0;">Details For Invoice # <?php echo $booking['InvoiceNumber']; ?></h4>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Total Service Cost</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo number_format($booking['ServiceCost'], 2); ?>
            SAR
        </td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Total Material Cost</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['MaterialCost']; ?> SAR</td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Coupon Code Used</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo($booking['CouponCode'] != '' ? $booking['CouponCode'] : 'N/A'); ?></td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Coupon Code Discount %</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo($booking['CouponCodeDiscountPercentage'] != '' ? $booking['CouponCodeDiscountPercentage'] : 'N/A'); ?></td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Loyalty Points Used</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo($booking['LoyaltyPointsUsed'] != '' ? $booking['LoyaltyPointsUsed'] : 'N/A'); ?></td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Earned Loyalty Points</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo($booking['EarnedLoyaltyPoints'] != '' ? $booking['EarnedLoyaltyPoints'] : 'N/A'); ?></td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Discount Availed</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo number_format($booking['DiscountAvailed'], 2); ?>
            SAR
        </td>
        <td style="padding: 10px 16px;border: 1px solid #999;">VAT Applied</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo number_format($booking['VatApplied'], 2); ?>
            SAR
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 16px;border: 1px solid #999;">Total Cost</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo number_format(($booking['ServiceCost'] + $booking['MaterialCost'] + $booking['VatApplied'] - $booking['DiscountAvailed']), 2); ?>
            SAR
        </td>
        <td style="padding: 10px 16px;border: 1px solid #999;">Guarantee Date</td>
        <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $booking['GuaranteeDate'] != '' ? getFormattedDateTime($booking['GuaranteeDate'], 'l\, F jS\, Y') : 'N/A'; ?></td>
    </tr>
    </tbody>
</table>
<table>
    <tbody>
    <tr>
        <td>
            <table style="width: 100%;margin: 2px auto;border-collapse: collapse;table-layout: fixed; font-size: 14px;">
                <tbody>
                <tr>
                    <td colspan="3" style="text-align: center;padding: 15px 0;border: 1px solid #999;">
                        <h4 style="margin: 0;">Services Used</h4>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px 16px;border: 1px solid #999;">#</td>
                    <td style="padding: 10px 16px;border: 1px solid #999;">Service Description</td>
                    <td style="padding: 10px 16px;border: 1px solid #999;">Service Cost</td>
                </tr>
                <?php if ($booking['booking_services'] && count($booking['booking_services']) > 0) {
                    $i = 1;
                    foreach ($booking['booking_services'] as $service) { ?>
                        <tr>
                            <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $i; ?></td>
                            <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $service->ServiceDescription; ?></td>
                            <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $service->ServiceCost; ?> SAR</td>
                        </tr>
                        <?php
                        $i++;
                    }
                } else { ?>
                    <tr>
                        <td colspan="3" style="padding: 10px 16px;border: 1px solid #999;text-align: center;">No data to show</td>
                    </tr>
                <?php }
                ?>
                </tbody>
            </table>
        </td>
        <td>
            <table style="width: 100%;margin: 2px auto;border-collapse: collapse;table-layout: fixed; font-size: 14px;">
                <tbody>
                <tr>
                    <td colspan="3" style="text-align: center;padding: 15px 0;border: 1px solid #999;">
                        <h4 style="margin: 0;">Materials Used</h4>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px 16px;border: 1px solid #999;">#</td>
                    <td style="padding: 10px 16px;border: 1px solid #999;">Material Description</td>
                    <td style="padding: 10px 16px;border: 1px solid #999;">Material Cost</td>
                </tr>
                <?php if ($booking['booking_materials'] && count($booking['booking_materials']) > 0) {
                    $i = 1;
                    foreach ($booking['booking_materials'] as $material) { ?>
                        <tr>
                            <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $i; ?></td>
                            <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $material->MaterialDescription; ?></td>
                            <td style="padding: 10px 16px;border: 1px solid #999;"><?php echo $material->MaterialCost; ?> SAR</td>
                        </tr>
                        <?php
                        $i++;
                    }
                } else { ?>
                    <tr>
                        <td colspan="3" style="padding: 10px 16px;border: 1px solid #999;text-align: center;">No data to show</td>
                    </tr>
                <?php }
                ?>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>