<?php
$promoted_products_arr = array();
if($promoted_products){
    foreach ($promoted_products as $promoted_product) {
        $promoted_products_arr[] = $promoted_product->ProductID;
    }
}

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!file_exists($result[0]->PromotionImage)) {
    $image = base_url("assets/backend/img/no_img.png");
} else {
    $image = base_url($result[0]->PromotionImage);
}

if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        $common_fields5 = '';
        if ($key == 0) {

            $common_fields = '<div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="SortOrder' . $key . '">Sort Order</label>
                                                                <input type="text" name="SortOrder" parsley-trigger="change" required  class="form-control number-only" id="SortOrder' . $key . '" value="' . ((isset($result[$key]->SortOrder)) ? $result[$key]->SortOrder : '') . '">
                                                               
                                                            </div>
                                                        </div>';

            $common_fields2 = '<div class="col-md-6">
                                <img class="img" src="' . $image . '" alt="" style="width:100px;height:100px;">
                                    <div class="form-group"><label>Image</label>
                                         <input type="file" name="PromotionImage[]"  required accept="image/*">
                                    </div>
                                </div>
                                <div class="col-md-4 checkbox-radios">

                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                            </div>';
            $common_fields5 = '<div class="col-md-12 by_url" style="display: ' . ((isset($result[$key]->PromotionType) && $result[$key]->PromotionType == 2) ? 'block' : 'none') . ';">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="PromotionUrl">Promotion Url</label>
                                        <input type="text" name="PromotionUrl" class="form-control" id="PromotionUrl" value="' . ((isset($result[$key]->PromotionUrl)) ? $result[$key]->PromotionUrl : '') . '">
                                    </div>
                                </div>';                
            $common_fields4 = '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="PromotionType">
                                                <input name="PromotionType" value="0" type="radio" id="PromotionTypeNothing" ' . ((isset($result[$key]->PromotionType) && $result[$key]->PromotionType == 0) ? 'checked' : '') . ' />Nothing &nbsp;&nbsp;
                                            </label>
                                            <label for="PromotionType">
                                                <input name="PromotionType" value="1" type="radio" id="PromotionTypeProduct" ' . ((isset($result[$key]->PromotionType) && $result[$key]->PromotionType == 1) ? 'checked' : '') . ' />Sponsored By Product &nbsp;&nbsp;
                                            </label>
                                            <label for="PromotionType">
                                                <input name="PromotionType" value="2" type="radio" id="PromotionTypeUrl" ' . ((isset($result[$key]->PromotionType) && $result[$key]->PromotionType == 2) ? 'checked' : '') . ' />Sponsored By Url
                                            </label>
                                        </div>
                                    </div>
                                </div>';                

            $common_fields3 = ''.$common_fields5.' <div class="col-md-12 by_product" style="display:' . ((isset($result[$key]->PromotionType) && $result[$key]->PromotionType == 1) ? 'block' : 'none') . '">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Products">Products <small style="color: red;">(Only 1 product per booth allowed)</small></label>
                                        <select id="Products" class="selectpicker" data-style="select-with-transition"
                                                required name="Products[]">';
            if (!empty($products)) {
                foreach ($products as $product) {
                    if (in_array($product->ProductID, $promoted_products_arr)) {
                        $selected = 'selected';
                    } else {
                        $selected = '';
                    }

                    $common_fields3 .= '<option value="' . $product->ProductID . '" ' . $selected . '>' . $product->Title . '(' . $product->BoothName . ') </option>';
                }
            }
            $common_fields3 .= '</select>
                                    </div>
                                </div><br>';
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                    <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                        <input type="hidden" name="form_type" value="update">
                        <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                        <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                        <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                    <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                </div>
                            </div>
                            '.$common_fields4.'
                            
                                    
                        </div>
                        <div class="row">
                        ' . $common_fields . '
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">Description</label>
                                        <textarea class="form-control textarea" name="Description" id="Description" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>

                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            ' . $common_fields3 . '
                            ' . $common_fields2 . '
                        </div>
                        <div class="form-group text-right m-b-0">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                            </button>
                            <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                <button type="button" class="btn btn-default waves-effect m-l-5">
                                        ' . lang('back') . '
                                </button>
                            </a>
                        </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit Sponsored - <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script src="<?php echo base_url(); ?>assets/backend/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/tinymce_custom.js"></script>-->
<script>
    $(document).ready(function(){
        $('#PromotionTypeProduct').on('click',function(){
            $('.by_url').hide();
            $('.by_product').show();
        });
        $('#PromotionTypeUrl').on('click',function(){
            $('.by_product').hide();
            $('.by_url').show();
        });
         $('#PromotionTypeNothing').on('click',function(){
            $('.by_product').hide();
            $('.by_url').hide();
        });

    })

</script>