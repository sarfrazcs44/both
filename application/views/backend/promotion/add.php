<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Add Sponsored</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="PromotionType">
                                                <input name="PromotionType" value="0" type="radio" id="PromotionTypeNothing" />Nothing &nbsp;&nbsp;
                                            </label>
                                            <label for="PromotionType">
                                                <input name="PromotionType" value="1" type="radio" id="PromotionTypeProduct"/>Sponsored By Product &nbsp;&nbsp;
                                            </label>
                                            <label for="PromotionType">
                                                <input name="PromotionType" value="2" type="radio" id="PromotionTypeUrl" />Sponsored By Url
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">Description</label>
                                        <textarea class="form-control" name="Description" id="Description" style="height: 100px;"></textarea>

                                    </div>
                                </div>
                                <div class="col-md-12 by_product" style="display: none;">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Products">Products <small style="color: red;">(Only 1 product per booth allowed)</small></label>
                                        <select id="Products" class="selectpicker" data-style="select-with-transition"
                                                required name="Products[]">
                                            <?php if (!empty($products)) {
                                                foreach ($products as $product) { ?>
                                                    <option value="<?php echo $product->ProductID; ?>"><?php echo $product->Title; ?> (<?php echo $product->BoothName; ?>)</option>
                                                <?php }
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-12 by_url" style="display: none;">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="PromotionUrl">Promotion Url</label>
                                        <input type="text" name="PromotionUrl" required  class="form-control" id="PromotionUrl">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <div class="col-md-6">
                                    <div class="form-group"><label>Image</label>
                                         <input type="file" name="PromotionImage[]"  required accept="image/*">
                                    </div>
                                </div>

                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<!--<script src="<?php echo base_url();?>assets/backend/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/tinymce_custom.js"></script>-->
<script>
    $(document).ready(function(){
        $('#PromotionTypeProduct').on('click',function(){
            $('.by_url').hide();
            $('.by_product').show();
        });
        $('#PromotionTypeUrl').on('click',function(){
            $('.by_product').hide();
            $('.by_url').show();
        });
         $('#PromotionTypeNothing').on('click',function(){
            $('.by_product').hide();
            $('.by_url').hide();
        });

    })

</script>