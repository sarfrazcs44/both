<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $result->StyleTitleEn; ?> - Bottoms</h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/' . $ControllerName . '/addBottom/' . $main_image_id . '/' . $result->ThemeStyleID); ?>">
                                <button type="button"
                                        class="btn btn-primary waves-effect w-md waves-light m-b-5">
                                    Add Bottom
                                </button>
                            </a>
                            <button type="button" class="btn btn-default waves-effect m-l-5" onclick="window.history.back();">
                                <?php echo lang('back');?>
                            </button>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Title En</th>
                                    <th>Title Ar</th>
                                    <th>Style Image</th>
                                    <th><?php echo lang('is_active'); ?></th>
                                    <th><?php echo lang('actions');?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($results) {
                                    foreach ($results as $value) { ?>
                                        <tr id="<?php echo $value->ThemeStyleID; ?>">
                                            <td>
                                                <?php echo $value->StyleTitleEn; ?>
                                            </td>
                                            <td><?php echo $value->StyleTitleAr; ?></td>
                                            <td style="width: 50%;">
                                                <img src="<?php echo base_url($value->StyleImage); ?>"
                                                     alt="<?php echo $value->StyleTitleEn; ?>">
                                            </td>
                                            <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/'.$ControllerName.'/editStyle/'.$value->ThemeStyleID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>