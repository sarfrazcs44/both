<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">visibility</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">
                            Orders <?php echo $this->uri->segment(3) != '' ? '- ' . ucwords(str_replace('_', ' ', ucwords($this->uri->segment(3)))) : ''; ?></h4>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer Name</th>
                                    <th>Booth Name</th>
                                    <th>Order Tracking #</th>
                                    <?php if($this->uri->segment(3) == 'dispatched'){ ?>
                                    <th>Delivery Code</th>
                                <?php } ?>
                                    <?php if (isset($all)) { ?>
                                        <th>Order Status</th>
                                    <?php } ?>
                                    <th>Order Grand Total</th>
                                    <th>Order Received At</th>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($results) {
                                    $i = 1;
                                    foreach ($results as $value) { ?>
                                        <tr id="<?php echo $value['OrderRequestID']; ?>">
                                            <td><?php echo $i; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['UserID']); ?>"
                                                   target="_blank"><?php echo $value['FullName']; ?></a></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                                   target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                            <td><?php echo $value['OrderTrackID']; ?></td>

                                            <?php if($this->uri->segment(3) == 'dispatched'){ ?>
                                                <td><?php echo $value['OrderRequestVerificationCode']; ?></td>
                                            <?php } ?>
                                            <?php if (isset($all)) {
                                                $status = $value['OrderStatusID'];
                                                if ($status == 1) {
                                                    $class = "btn btn-sm";
                                                } else if ($status == 2) {
                                                    $class = "btn btn-warning btn-sm";
                                                } else if ($status == 3 || $status == 7) {
                                                    $class = "btn btn-info btn-sm";
                                                } else if ($status == 4) {
                                                    $class = "btn btn-success btn-sm";
                                                } else if ($status == 5 || $status == 6 || $status == 8) {
                                                    $class = "btn btn-danger btn-sm";
                                                }
                                                ?>
                                                <td>
                                                    <button class="<?php echo $class; ?>"><?php echo $value['OrderStatus']; ?>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </td>
                                            <?php } ?>
                                            <td><?php echo $value['GrandTotal']; ?> <?php echo $value['BoothCurrency']; ?></td>
                                            <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                            <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <!--<a href="<?php /*echo base_url('cms/' . $ControllerName . '/invoice/' . $value['OrderRequestID']); */ ?>?type=view"
                                                           class="btn btn-simple btn-warning btn-icon" target="_blank"><i
                                                                    class="material-icons"
                                                                    title="Click to view invoice">receipt</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <a href="<?php /*echo base_url('cms/' . $ControllerName . '/invoice/' . $value['OrderRequestID']); */ ?>?type=download"
                                                           class="btn btn-simple btn-warning btn-icon" target="_blank"><i
                                                                    class="material-icons"
                                                                    title="Click to download invoice">save_alt</i>
                                                            <div class="ripple-container"></div>
                                                        </a>-->
                                                    <?php } ?>

                                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . $value['OrderRequestID']); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons"
                                                                    title="Click to view details">visibility</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>

                                                     <a href="javascript:void(0);"
                                                           class="btn btn-simple btn-warning btn-icon send_sms" data-user-id="<?php echo $value['UserID']; ?>" data-order-track-id="<?php echo $value['OrderTrackID']; ?>" data-verfication-code="<?php echo $value['OrderRequestVerificationCode']; ?>" data-booth-name="<?php echo $value['BoothName']; ?>">Resend Code
                                                            
                                                        </a>

                                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                        <!--<a href="javascript:void(0);"
                                                           onclick="deleteRecord('<?php /*echo $value['OrderRequestID']; */ ?>','cms/<?php /*echo $ControllerName; */ ?>/action','')"
                                                           class="btn btn-simple btn-danger btn-icon remove"><i
                                                                    class="material-icons" title="Delete">close</i>
                                                            <div class="ripple-container"></div>
                                                        </a>-->
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        $i++;
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {

        $('.send_sms').on('click',function(){

            var UserID = $(this).attr('data-user-id');
            var OrderTrackID = $(this).attr('data-order-track-id');
            var VerificationCode = $(this).attr('data-verfication-code');
            var BoothName = $(this).attr('data-booth-name');

            $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/order/SendOrderDispatchedSMSToCustomer',
                        data: {

                            'UserID' : UserID,
                            'OrderTrackID' : OrderTrackID,
                            'VerificationCode' : VerificationCode,
                            'BoothName' : BoothName
                            
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

           
            

        });
        
    });
</script>