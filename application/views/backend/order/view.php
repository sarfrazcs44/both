<?php $order_request = convertEmptyToNA($order_request); ?>
<?php
$status = $order_request['OrderStatusID'];
if ($status == 1) {
    $class = "btn btn-sm";
} else if ($status == 2) {
    $class = "btn btn-warning btn-sm";
} else if ($status == 3 || $status == 7) {
    $class = "btn btn-info btn-sm";
} else if ($status == 4) {
    $class = "btn btn-success btn-sm";
} else if ($status == 5 || $status == 6 || $status == 8 || $status == 9) {
    $class = "btn btn-danger btn-sm";
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="row">
                                <div class="col-sm-4">
                                    <h5 class="card-title">Customer Details</h5>
                                </div>
                                <div class="col-sm-8 pull-right">
                                    <button class="btn btn-danger btn-sm"
                                            onclick="CancelOrderRequest('<?php echo $order_request['OrderRequestID']; ?>');">
                                        Cancel Order
                                        <div class="ripple-container"></div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Customer Name</label>
                                        <h5>
                                            <a href="<?php echo base_url('cms/user/view/' . base64_encode($order_request['UserID'])); ?>"
                                               target="_blank"><?php echo $order_request['FullName']; ?></a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Customer User Name</label>
                                        <h5><?php echo $order_request['UserName']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Customer Email</label>
                                        <h5><?php echo $order_request['UserEmail']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Customer Mobile</label>
                                        <h5><?php echo $order_request['UserMobile']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Customer City</label>
                                        <h5><?php echo $order_request['UserCityTitle']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Customer Gender</label>
                                        <h5><?php echo $order_request['UserGender']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Email Verified?</label>
                                        <h5><?php echo $order_request['IsEmailVerified'] == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Mobile Verified?</label>
                                        <h5><?php echo $order_request['IsMobileVerified'] == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Online Status</label>
                                        <h5><?php echo $order_request['OnlineStatus']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Order Received At</label>
                                        <h5><?php echo $order_request['OrderReceivedAt'] != '' ? getFormattedDateTime($order_request['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Order Status</label>
                                        <button class="<?php echo $class; ?>"><?php echo $order_request['OrderStatus']; ?>
                                            <div class="ripple-container"></div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Address Details </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Recipient Name</label>
                                        <h5><?php echo $order_request['RecipientName']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Recipient Email</label>
                                        <h5><?php echo $order_request['AddressEmail']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Recipient Mobile</label>
                                        <h5><?php echo $order_request['AddressMobile']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Recipient Gender</label>
                                        <h5><?php echo ucfirst($order_request['AddressGender']); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">City</label>
                                        <h5><?php echo $order_request['AddressCity']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Apartment No.</label>
                                        <h5><?php echo $order_request['ApartmentNo']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Address 1</label>
                                        <h5><?php echo $order_request['Address1']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Address 2</label>
                                        <h5><?php echo $order_request['Address2']; ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">See location on map</label>
                                        <a href="https://www.google.com/maps/search/?api=1&query=<?php echo $order_request['AddressLatitude']; ?>,<?php echo $order_request['AddressLongitude']; ?>"
                                           target="_blank">
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHbVVHxTASKeTMj0H-JFtHN138p-i6Rx-UdC0VQ2l17yJcaRFVCQ"
                                                 height="25" width="25"
                                                 style="height: 40px !important;width: 40px !important;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Order Details </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Seller Name</label>
                                        <h5>
                                            <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($order_request['BoothID']); ?>"
                                               target="_blank"><?php echo $order_request['BoothName']; ?></a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Seller Store Type</label>
                                        <h5>
                                            <?php echo $order_request['BoothType']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Order Track #</label>
                                        <h5>
                                            <?php echo $order_request['OrderTrackID']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Total Amount</label>
                                        <h5>
                                            <?php echo $order_request['TotalAmount']; ?><?php echo $order_request['OrderItems'][0]['Currency']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Actual Delivery Charges</label>
                                        <h5>
                                            <?php echo $order_request['ActualDeliveryCharges']; ?><?php echo $order_request['OrderItems'][0]['Currency']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Additional Delivery
                                            Charges</label>
                                        <h5>
                                            <?php echo number_format($order_request['AdditionalDeliveryCharges'], 2); ?><?php echo $order_request['OrderItems'][0]['Currency']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Discount</label>
                                        <h5>
                                            <?php echo $order_request['Discount']; ?><?php echo $order_request['OrderItems'][0]['Currency']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Promo Code Used</label>
                                        <h5>
                                            <?php echo($order_request['PromoCodeUsed'] != '' ? $order_request['PromoCodeUsed'] : 'N/A'); ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Promo Code Discount</label>
                                        <h5>
                                            <?php echo($order_request['PromoCodeUsed'] != '' ? $order_request['PromoCodeDiscount'] . ' ' . $order_request['OrderItems'][0]['Currency'] : 'N/A'); ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Vat Percentage Applied</label>
                                        <h5>
                                            <?php echo $order_request['VatPercentageApplied']; ?>%
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Vat Amount Applied</label>
                                        <h5>
                                            <?php echo $order_request['VatAmountApplied']; ?><?php echo $order_request['OrderItems'][0]['Currency']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Grand Total</label>
                                        <h5>
                                            <?php echo $order_request['GrandTotal']; ?> <?php echo $order_request['OrderItems'][0]['Currency']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Payment Method</label>
                                        <h5>
                                            <?php echo $order_request['PaymentMethod']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Order Request Verification
                                            Code</label>
                                        <h5>
                                            <?php echo($order_request['OrderRequestVerificationCode'] != '' ? $order_request['OrderRequestVerificationCode'] : 'N/A'); ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Order Request Rating</label>
                                        <h5>
                                            <?php echo($order_request['OrderRequestRating'] > 0 ? $order_request['OrderRequestRating'] . ' / 5' : 'N/A'); ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">User Rating</label>
                                        <h5>
                                            <?php echo($order_request['UserOrderRequestRating'] > 0 ? $order_request['UserOrderRequestRating'] . ' / 5' : 'N/A'); ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Ordered Items </h5>
                    </div>
                    <div class="card-content">
                        <div class="material-datatables">
                            <table class="table table-striped table-hover datatables"
                                   cellspacing="0" width="100%"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Product Image</th>
                                    <th>Product Title</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($order_request['OrderItems'])) {
                                    $j = 1;
                                    foreach ($order_request['OrderItems'] as $orderItem) {
                                        ?>
                                        <tr>
                                            <td><?php echo $j; ?></td>
                                            <td>
                                                <a data-fancybox="gallery"
                                                   href="<?php echo base_url($orderItem['ProductImage']); ?>">
                                                    <img src="<?php echo base_url($orderItem['ProductImage']); ?>"
                                                         style="width: 80px !important;height: 80px !important;"></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url('cms/product/view') . '/' . $orderItem['ProductID']; ?>"
                                                   target="_blank"><?php echo $orderItem['ProductTitle']; ?></a>
                                            </td>
                                            <td><?php echo $orderItem['Quantity']; ?></td>
                                            <td><?php echo $orderItem['Price']; ?><?php echo $orderItem['Currency']; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/product/view/' . $orderItem['ProductID']); ?>"
                                                   class="btn btn-simple btn-warning btn-icon edit"
                                                   target="_blank"><i
                                                            class="material-icons"
                                                            title="Click to view details">visibility</i>
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                        $j++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    function CancelOrderRequest(order_request_id) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/order/CancelOrderRequest',
                        data: {
                            'OrderRequestID': order_request_id
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }
</script>