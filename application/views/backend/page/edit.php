<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!file_exists($result[0]->Image)) {
    $image = base_url("assets/backend/img/no_img.png");
} else {
    $image = base_url($result[0]->Image);
}
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        if ($key == 0) {

            $common_fields2 = '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>';

            $common_fields3 = '<div class="row">
                                <div class="col-md-12">
                                    
                                    <img src="' . $image . '" style="width:100px;height:100px;">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group"><label>Image</label>
                                         <input type="file" name="Image[]"  required accept="image/*">
                                    </div>
                                </div>
                            </div>';

            if ($result[0]->$TableKey == 2)
            {
                $common_fields4 = '<div class="row">
                                 <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group"><label>Email</label>
                                         <input type="text" name="Email" class="form-control" value="'.$result[$key]->Email.'" required>
                                    </div>
                                </div>
                                 <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group"><label>Facebook Username</label>
                                         <input type="text" name="Facebook" class="form-control" value="'.$result[$key]->Facebook.'" required>
                                    </div>
                                </div>
                                <!--<div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group"><label>Linkedin Username</label>
                                         <input type="text" name="Linkedin" class="form-control" value="'.$result[$key]->Linkedin.'" required>
                                    </div>
                                </div>-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group"><label>Instagram Username</label>
                                         <input type="text" name="Instagram" class="form-control" value="'.$result[$key]->Instagram.'" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group"><label>Twitter Username</label>
                                         <input type="text" name="Twitter" class="form-control" value="'.$result[$key]->Twitter.'" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group"><label>Snapchat Username</label>
                                         <input type="text" name="Snapchat" class="form-control" value="'.$result[$key]->Snapchat.'" required>
                                    </div>
                                </div>
                            </div>';
            }
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                         ' . $common_fields2 . '
                                                    </div>
                                                    <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description' . $key . '">Description</label>
                                        <textarea class="form-control textarea" name="Description" id="Description' . $key . '" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>

                                    </div>
                                </div>
                            </div>
                            ' . $common_fields4 . '
                            ' . $common_fields3 . '
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/tinymce_custom.js"></script>