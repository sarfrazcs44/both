<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">face</i>
                    </div>
                    <div class="card-content">

                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#admins" data-toggle="tab" aria-expanded="true">Feedback</a>
                            </li>
                            <li class="">
                                <a href="#app_users" data-toggle="tab" aria-expanded="false">Complain</a>
                            </li>

                        </ul>

                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="admins">
                                <h4 class="card-title">Feedback</h4>
                                
                                <table id="" class="datatables table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Message</th>
                                    <th>Received At</th>
                                    <?php if(checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                        <th><?php echo lang('actions');?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){
                                        if($value->OrderNumber == ''){
                                        ?>
                                        <tr id="<?php echo $value->FeedbackID;?>">
                                            <td><?php echo $value->FullName; ?></td>
                                            <td><?php echo $value->Email; ?></td>
                                            <td><?php echo ($value->MobileNo !== '' ? $value->MobileNo : 'N/A'); ?></td>
                                            <td><?php echo $value->Message; ?></td>
                                            <td><?php echo getFormattedDateTime($value->CreatedAt, 'd-m-Y h:i A'); ?></td>
                                            <?php if(checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                <td>
                                                    <?php if(checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->FeedbackID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . base64_encode($value->FeedbackID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                            </div>
                            <div class="material-datatables tab-pane" id="app_users">
                                <h4 class="card-title">Complain</h4>
                               
                                <table id="" class="datatables table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Order Number</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Message</th>
                                    <th>Received At</th>
                                    <?php if(checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                        <th><?php echo lang('actions');?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){
                                         if($value->OrderNumber != ''){
                                        ?>
                                        <tr id="<?php echo $value->FeedbackID;?>">
                                             <td><?php echo $value->OrderNumber; ?></td>
                                            <td><?php echo $value->FullName; ?></td>
                                            <td><?php echo $value->Email; ?></td>
                                            <td><?php echo ($value->MobileNo !== '' ? $value->MobileNo : 'N/A'); ?></td>
                                            <td><?php echo $value->Message; ?></td>
                                            <td><?php echo getFormattedDateTime($value->CreatedAt, 'd-m-Y h:i A'); ?></td>
                                            <?php if(checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                <td>
                                                    <?php if(checkUserRightAccess(63,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->FeedbackID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . base64_encode($value->FeedbackID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                            </div>
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>