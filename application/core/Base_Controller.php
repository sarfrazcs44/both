<?php

class Base_Controller extends CI_Controller {

    protected $language;

    public function __construct() {

        parent::__construct();
        if ($this->session->userdata('lang')) {
            $this->language = $this->session->userdata('lang');
            // $this->language = $this->session->userdata('languageID');
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'EN';
            }
        }
        $this->data['site_setting'] = $this->getSiteSetting();
    }

    public function changeLanguage($language) {
        $this->load->Model('Model_system_language');
        $fetch_by['GlobalCode'] = $language;
        $result = $this->Model_system_language->getWithMultipleFields($fetch_by);
        $languageID = $result->SysLID;
        if (!$result) {

            $default_lang = getDefaultLanguage();
            $language = $default_lang->ShortCode;
            $languageID = $default_lang->SystemLanguageID;
        }
        $this->session->set_userdata('lang', $language);
        //$this->session->set_userdata('languageID',$languageID);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function getSiteSetting() {

        $this->load->model('Site_setting_model');
        return $this->Site_setting_model->get(1, false, 'SiteSettingID');
    }

    public function uploadImage($file_key, $path, $id = false, $type = false, $multiple = false) {
        $data = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
            $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
            $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {

                move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);
                if (!$multiple) {
                    return $path . $file_name;
                } else {
                    $this->load->model('Site_images_model');
                    $data['FileID'] = $id;
                    $data['ImageType'] = $type;
                    $data['ImageName'] = $path . $file_name;
                    $this->Site_images_model->save($data);
                }
                /* $data['DestinationID'] = $id; 
                  $data['ImagePath'] = $path.$file_name;
                  $this->Site_images_model->save($data); */
            }
        }
        return true;
    }
    
    public function DeleteImage() {
        $deleted_by = array();
        $ImagePath = $this->input->post('image_path');
        $deleted_by['SiteImageID'] = $this->input->post('image_id');
        if (file_exists($ImagePath)) {
            unlink($ImagePath);
        }
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }
    
}
