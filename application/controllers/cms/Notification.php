<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('User_model');
        $this->load->Model('User_text_model');
        $this->load->Model('City_model');
        $this->load->Model('Role_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';
    }

    public function index()
    {
        $this->data['roles'] = $this->Role_model->getAllJoinedData(false, 'RoleID', $this->language, 'roles.IsActive = 1');
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function sendNotification()
    {
        $post_data = $this->input->post();
        $notification_type = $post_data['NotificationType'];
        $RoleID = $post_data['RoleID'];
        $title = $post_data['Title'];
        $description = $post_data['Description'];
        $data = array();

        $users = $this->User_model->getJoinedData(true, 'UserID', 'users.RoleID = ' . $RoleID);
        if ($users) {
            foreach ($users as $user) {
                if ($notification_type == 'PushNotification') {
                    sendNotification($title, $description, $data, $user['UserID']);
                } elseif ($notification_type == 'Email') {
                    $data['to'] = $user['Email'];
                    $data['subject'] = $title;
                    $data['message'] = email_format($description);
                    sendEmail($data);
                } elseif ($notification_type == 'SMS') {
                    $sms_text = $title . "\n" . $description;
                    sendSms($user['Mobile'], $sms_text);
                }
            }
        }

        $success['error'] = false;
        $success['success'] = 'Notification sent successfully';
        $success['redirect'] = false;
        echo json_encode($success);
        exit;
    }

}