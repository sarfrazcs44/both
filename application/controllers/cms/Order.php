<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('Order_model');
        $this->load->Model('Order_request_model');
        $this->load->Model('Order_item_model');
        $this->load->Model('User_model');
        $this->load->Model('City_model');

        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'OrderID';
        $this->data['Table'] = 'orders';
    }

    public function index()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['results'] = $this->Order_request_model->getOrdersRequests();
        $this->data['all'] = 1;
        // dump($this->data['results']);
        $this->load->view('backend/layouts/default', $this->data);
    }

    

    public function pending_approval()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "orders_requests.OrderStatusID = 1";
        $this->data['results'] = $this->Order_request_model->getOrdersRequests($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function pending_payment()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "orders_requests.OrderStatusID = 2";
        $this->data['results'] = $this->Order_request_model->getOrdersRequests($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function payment_done()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "orders_requests.OrderStatusID = 3";
        $this->data['results'] = $this->Order_request_model->getOrdersRequests($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function dispatched()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "orders_requests.OrderStatusID = 7";
        $this->data['results'] = $this->Order_request_model->getOrdersRequests($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function completed()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "orders_requests.OrderStatusID = 4";
        $this->data['results'] = $this->Order_request_model->getOrdersRequests($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function seller_cancelled()
    {
        $this->data['completed_bookings'] = true;
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "orders_requests.OrderStatusID = 5";
        $this->data['results'] = $this->Order_request_model->getOrdersRequests($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function buyer_cancelled()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "orders_requests.OrderStatusID = 6";
        $this->data['results'] = $this->Order_request_model->getOrdersRequests($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function admin_cancelled()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "orders_requests.OrderStatusID = 8";
        $this->data['results'] = $this->Order_request_model->getOrdersRequests($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function view($OrderRequestID)
    {
        $order_request = $this->Order_request_model->getOrdersRequests("orders_requests.OrderRequestID = " . $OrderRequestID, $this->language, false, 0, 'orders_requests.OrderRequestID', 'ASC');
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/view';
        if (!$order_request) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        if ($order_request) {
            $order_request = $order_request[0];
            $order_items = $this->Order_item_model->getOrderItems("order_items.OrderRequestID = " . $order_request['OrderRequestID']);
            if ($order_items) {
                $i = 0;
                foreach ($order_items as $order_item) {
                    $product_images = getProductImages($order_item['ProductID']);
                    $order_request['OrderItems'][$i] = $order_item;
                    $order_request['OrderItems'][$i]['ProductImage'] = $product_images[0]->ProductCompressedImage;
                    $i++;
                }
            } else {
                $order_request['OrderItems'] = array();
            }
        }
        $this->data['order_request'] = $order_request;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;

        }
    }



    public function SendOrderDispatchedSMSToCustomer()
    {
        $UserID = $this->input->post('UserID');
        $OrderTrackID = $this->input->post('OrderTrackID');
        $VerificationCode = $this->input->post('VerificationCode');
        $BoothName = $this->input->post('BoothName');
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID, $this->language);
       

        // Sending sms to customer

        if($user_info['LastLangState'] == 'EN'){
            $message = "Hi, your order #".$OrderTrackID." from ".$BoothName." has been dispatched! Please submit the delivery code(".$VerificationCode.") to the booth once you receive your order to mark the order completed.";
        }else{
            $message = "
مرحبا، طلبك #".$OrderTrackID." من بوث ".$BoothName." في الطريق اليك!
الرجاء تسليم كود التوصيل(".$VerificationCode.") للبوث عند استلام طلبك ليتم اغلاق الطلب!";
        }

        sendSms($user_info['Mobile'], $message);
        $success['error'] = false;
        $success['success'] = 'Sent Successfully';

        echo json_encode($success);
        exit;
    }

    public function update()
    {
        $post_data = $this->input->post();
        if (isset($post_data['BookingID'])) {
            $BookingID = $post_data['BookingID'];
            unset($post_data['BookingID']);
            if (isset($post_data['SendAssignmentEmail'])) {
                $SendAssignmentEmail = $post_data['SendAssignmentEmail'];
                unset($post_data['SendAssignmentEmail']);
            }
            if (isset($post_data['SendCancelBookingNotification'])) {
                $SendCancelBookingNotification = $post_data['SendCancelBookingNotification'];
                unset($post_data['SendCancelBookingNotification']);
            }
            // dump($post_data);
            $this->Booking_model->update($post_data, array('BookingID' => $BookingID));
            $booking_info = $this->Booking_model->getBookings('bookings.BookingID = ' . $BookingID);
            $booking_info = $booking_info[0];
            if (isset($SendAssignmentEmail) && $SendAssignmentEmail == 'yes') {
                $this->SendAssignmentPushNotificationToTechnician($booking_info);
                $this->SendAssignmentSmsToTechnician($booking_info);
                $this->SendAssignmentEmailToTechnician($booking_info);
                $this->SendAssignmentPushNotificationToCustomer($booking_info);
                $this->SendAssignmentSmsToCustomer($booking_info);
                $this->SendAssignmentEmailToCustomer($booking_info);
                unset($post_data['SendAssignmentEmail']);
            }
            if (isset($SendCancelBookingNotification) && $SendCancelBookingNotification == 'yes') {
                $this->SendCancelledPushNotificationToCustomer($booking_info);
                $this->SendCancelledSmsToCustomer($booking_info);
                $this->SendCancelledEmailToCustomer($booking_info);
                unset($post_data['SendCancelBookingNotification']);
            }
            // echo $this->db->last_query();exit();
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/view/' . $BookingID;
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }


    private function delete()
    {
        if (!checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->Booking_model->delete($deleted_by);

        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    private function SendAssignmentPushNotificationToTechnician($booking_info)
    {
        $title = "Order # " . $booking_info['OrderNumber'] . " assigned at MPKSA";
        $description = "Dear " . $booking_info['TechnicianFullName'] . "\nAn order is assigned to you by admin with Order # " . $booking_info['OrderNumber'] . ".";
        $data = array();
        sendNotification($title, $description, $data, $booking_info['TechnicianID']);

        // notification data
        $notification_data['UserID'] = $booking_info['TechnicianID'];
        $notification_data['NotificationType'] = 'order_assigned';
        $notification_data['NotificationTitleEn'] = $title;
        $notification_data['NotificationTextEn'] = $description;
        $notification_data['NotificationTitleAr'] = "أمر رقم " . $booking_info['OrderNumber'] . " في MPKSA";
        $notification_data['NotificationTextAr'] = "عزيزي " . $booking_info['TechnicianFullName'] . "\n تم تعيين أمر لك من قبل المشرف مع الطلب رقم " . $booking_info['OrderNumber'] . ".";
        $notification_data['BookingID'] = $booking_info['BookingID'];
        $notification_data['NotificationCreatedAt'] = time();
        log_notification($notification_data);
    }

    private function SendAssignmentSmsToTechnician($booking_info)
    {
        // send sms to technician
        if (isset($booking_info['TechnicianMobile']) && $booking_info['TechnicianMobile'] != '') {
            $msg = "Dear " . $booking_info['TechnicianFullName'] . "\nAn order is assigned to you by admin with Order # " . $booking_info['OrderNumber'] . ".";
            sendSms($booking_info['TechnicianMobile'], $msg);
        }
    }

    private function SendAssignmentEmailToTechnician($booking_info)
    {
        if ($booking_info['TechnicianEmail'] !== '') {
            $data['from'] = 'no-reply@mpksa.com.sa';
            $data['to'] = $booking_info['TechnicianEmail'];
            $data['subject'] = "Order # " . $booking_info['OrderNumber'] . " assigned at MPKSA";
            $data['message'] = "Dear " . $booking_info['TechnicianFullName'] . "\nAn order is assigned to you by admin with Order # " . $booking_info['OrderNumber'] . ".";
            sendEmail($data);
        }
    }

    private function SendAssignmentPushNotificationToCustomer($booking_info)
    {
        $title = "Order # " . $booking_info['OrderNumber'] . " assigned at MPKSA";
        $description = "Dear " . $booking_info['FullName'] . "\nYour order with Order # " . $booking_info['OrderNumber'] . " is assigned to technician. He will soon be at your door step.";
        $data = array();
        sendNotification($title, $description, $data, $booking_info['UserID']);

        // notification data
        $notification_data['UserID'] = $booking_info['UserID'];
        $notification_data['NotificationType'] = 'order_assigned';
        $notification_data['NotificationTitleEn'] = $title;
        $notification_data['NotificationTextEn'] = $description;
        $notification_data['NotificationTitleAr'] = "أمر رقم " . $booking_info['OrderNumber'] . " تم تعيينه في MPKSA";
        $notification_data['NotificationTextAr'] = "عزيزي " . $booking_info['FullName'] . "\n يتم تعيين طلبك مع Order # " . $booking_info['OrderNumber'] . " إلى فني. سيكون قريبا في خطوة بابك.";
        $notification_data['BookingID'] = $booking_info['BookingID'];
        $notification_data['NotificationCreatedAt'] = time();
        log_notification($notification_data);
    }

    private function SendAssignmentSmsToCustomer($booking_info)
    {
        // send sms to technician
        if (isset($booking_info['Mobile']) && $booking_info['Mobile'] != '') {
            $msg = "Dear " . $booking_info['FullName'] . "\nYour order with Order # " . $booking_info['OrderNumber'] . " is assigned to technician. He will soon be at your door step.";
            sendSms($booking_info['Mobile'], $msg);
        }
    }

    private function SendAssignmentEmailToCustomer($booking_info)
    {
        if ($booking_info['Email'] !== '') {
            $data['from'] = 'no-reply@mpksa.com.sa';
            $data['to'] = $booking_info['Email'];
            $data['subject'] = "Order # " . $booking_info['OrderNumber'] . " assigned at MPKSA";
            $data['message'] = "Dear " . $booking_info['FullName'] . "\nYour order with Order # " . $booking_info['OrderNumber'] . " is assigned to technician. He will soon be at your door step.";
            sendEmail($data);
        }
    }

    private function SendCancelledPushNotificationToCustomer($booking_info)
    {
        $title = "Order # " . $booking_info['OrderNumber'] . " cancelled at MPKSA";
        $description = "Dear " . $booking_info['FullName'] . "\nYour order with Order # " . $booking_info['OrderNumber'] . " is cancelled by admin. We regret any inconvenience. Please contact admin for details.";
        $data = array();
        sendNotification($title, $description, $data, $booking_info['UserID']);

        // notification data
        $notification_data['UserID'] = $booking_info['UserID'];
        $notification_data['NotificationType'] = 'order_cancelled';
        $notification_data['NotificationTitleEn'] = $title;
        $notification_data['NotificationTextEn'] = $description;
        $notification_data['NotificationTitleAr'] = "تم إلغاء الطلب رقم " . $booking_info['OrderNumber'] . " في MPKSA";
        $notification_data['NotificationTextAr'] = "عزيزي " . $booking_info['FullName'] . "\n يتم إلغاء طلبك مع Order # " . $booking_info['OrderNumber'] . " بواسطة المسؤول. نأسف لأي إزعاج. يرجى الاتصال المشرف للحصول على التفاصيل.";
        $notification_data['BookingID'] = $booking_info['BookingID'];
        $notification_data['NotificationCreatedAt'] = time();
        log_notification($notification_data);
    }

    private function SendCancelledSmsToCustomer($booking_info)
    {
        // send sms to technician
        if (isset($booking_info['Mobile']) && $booking_info['Mobile'] != '') {
            $msg = "Dear " . $booking_info['FullName'] . "\nYour order with Order # " . $booking_info['OrderNumber'] . " is cancelled by admin. We regret any inconvenience. Please contact admin for details.";
            sendSms($booking_info['Mobile'], $msg);
        }
    }

    private function SendCancelledEmailToCustomer($booking_info)
    {
        if ($booking_info['Email'] !== '') {
            $data['from'] = 'no-reply@mpksa.com.sa';
            $data['to'] = $booking_info['Email'];
            $data['subject'] = "Order # " . $booking_info['OrderNumber'] . " cancelled at MPKSA";
            $data['message'] = "Dear " . $booking_info['FullName'] . "\nYour order with Order # " . $booking_info['OrderNumber'] . " is cancelled by admin. We regret any inconvenience. Please contact admin for details.";
            sendEmail($data);
        }
    }

    public function invoice($booking_id)
    {
        $fetch_by['BookingID'] = $booking_id;
        $booking_detail = $this->Booking_model->getBookings('bookings.BookingID = ' . $booking_id);
        $booking_detail = $booking_detail[0];
        $boking_materials = $this->Booking_material_model->getMultipleRows($fetch_by);
        $booking_services = $this->Booking_service_model->getMultipleRows($fetch_by);
        $booking_detail['booking_materials'] = ($boking_materials ? $boking_materials : array());
        $booking_detail['booking_services'] = ($booking_services ? $booking_services : array());
        $data['booking'] = $booking_detail;
        if (isset($_GET['type']) && $_GET['type'] == 'download') {
            $html = $this->load->view('backend/templates/invoice', $data, true);
            generate_pdf($html, $booking_detail['OrderNumber']);
        } else {
            $this->load->view('backend/templates/invoice', $data);
        }
    }

    public function markAsRead()
    {
        if (isset($_GET['BookingID']) && $_GET['BookingID'] > 0) {
            $this->Booking_model->update(array('IsRead' => 1), array('BookingID' => $_GET['BookingID']));
        } else {
            $this->Booking_model->update(array('IsRead' => 1), array('IsRead' => 0));
        }

        echo 1;
        exit();
    }

    public function CancelOrderRequest()
    {
        $post_data = $this->input->post();
        $where = 'orders_requests.OrderRequestID = ' . $post_data['OrderRequestID'];
        $order_request = $this->Order_request_model->getOrderRequestsForCronjob($where);
        $order_request = $order_request[0];
        $update['OrderLastStatusID'] = $order_request->OrderStatusID;
        $update['OrderStatusID'] = 8;
        $update['OrderCancellationReasonID'] = 1;
        $update_by['OrderRequestID'] = $post_data['OrderRequestID'];
        $this->Order_request_model->update($update, $update_by);


        // Sending notification To Customer
        $data['LoggedInUserID'] = $order_request->UserID; // the person who will receive this notification
        $data['UserType'] = 'user';
        $data['Type'] = 'order';
        $data['UserID'] = $order_request->BoothID; // the person who is making this notification
        $data['OrderID'] = $order_request->OrderID;
        $data['OrderRequestID'] = $order_request->OrderRequestID;
        $data['NotificationTextEn'] = "Admin has cancelled your order request with order track number " . $order_request->OrderTrackID;
        $data['NotificationTextAr'] = " قام البائع بإلغاء طلب طلبك مع رقم تتبع الطلب " . $order_request->OrderTrackID;
        $data['CreatedAt'] = time();
        log_notification($data);
        $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->UserID);
        $this->SendOrderCancelEmailToCustomer($order_request->UserID, $order_request->OrderTrackID);

        // Sending notification To Booth
        $data['LoggedInUserID'] = $order_request->BoothID; // the person who will receive this notification
        $data['UserType'] = 'booth';
        $data['Type'] = 'order';
        $data['UserID'] = $order_request->UserID; // the person who is making this notification
        $data['OrderID'] = $order_request->OrderID;
        $data['OrderRequestID'] = $order_request->OrderRequestID;
        $data['NotificationTextEn'] = "Admin has cancelled an order request with order track number " . $order_request->OrderTrackID;
        $data['NotificationTextAr'] = "قام المشتري بإلغاء طلب طلب برقم تتبع الطلب " . $order_request->OrderTrackID;
        $data['CreatedAt'] = time();
        log_notification($data);
        $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->BoothID);
        $this->SendOrderCancelEmailToBooth($order_request->BoothID, $order_request->OrderTrackID);

        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;
    }

    private function SendOrderCancelEmailToCustomer($UserID, $OrderTrackID)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID);
        $email_template = get_email_template(20, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{user_name}}", $user_info['UserName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
        //sendSms($user_info['Mobile'], $message);
    }

    private function SendOrderCancelEmailToBooth($UserID, $OrderTrackID)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID);
        $email_template = get_email_template(20, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{user_name}}", $user_info['BoothName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
        //sendSms($user_info['Mobile'], $message);
    }


}