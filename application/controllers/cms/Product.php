<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model',
            'Category_model',
            'Product_image_model',
            'Product_like_model',
            'Product_comment_model',
            'Product_reported_model',
            'Product_view_model',
            'Temp_order_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'ProductID';
        $this->data['Table'] = 'products';


    }


    public function index()
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['results'] = $this->$parent->getProducts(false, $this->language);
        $this->data['results_approved'] = $this->$parent->getProducts("products.IsPromotedProduct = 1 AND products.IsPromotionApproved = 1", $this->language);
        $this->data['results_pending'] = $this->$parent->getProducts("products.IsPromotedProduct = 1 AND products.IsPromotionApproved = 0", $this->language);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';

        $this->data['categories'] = $this->Category_model->getAllJoinedData(false, 'CategoryID', $this->language);

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['categories'] = $this->Category_model->getAllJoinedData(false, 'CategoryID', $this->language);


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }

    public function view($id = '')
    {
        if (!checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $product = $this->$parent->getProducts("products.ProductID = " . $id, $this->language);

        if (empty($product)) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $product = $product[0];
        $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $id));

        $likes_count = $this->Product_like_model->getCountProductLikes($id);
        $comments_count = $this->Product_comment_model->getProductCommentsCount($id);
        $reports_count = $this->Product_reported_model->getCountProductReports($id);
        $views_count = $this->Product_view_model->getProductsViewsCount($id);

        $likes = $this->Product_like_model->getProductLikes($id);
        $comments = $this->Product_comment_model->getProductComments("product_comments.ProductID = " . $id);
        $reports = $this->Product_reported_model->getProductReports($id);

        $product->ProductImages = $product_images ? $product_images : array();
        $product->LikesCount = $likes_count->Total;
        $product->CommentCount = $comments_count->Total;
        $product->ReportCount = $reports_count->Total;
        $product->ViewsCount = $views_count->Total;
        $product->ProductLikes = $likes ? $likes : array();
        $product->ProductComments = $comments ? $comments : array();
        $product->ProductReports = $reports ? $reports : array();
        $this->data['product'] = $product;

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/view';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {

        if (!checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = array();
        $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }


        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);


        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {


            $default_lang = getDefaultLanguage();


            $save_child_data['Title'] = $post_data['Title'];
            $save_child_data[$this->data['TableKey']] = $insert_id;
            $save_child_data['SystemLanguageID'] = $default_lang->SystemLanguageID;
            $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
            $this->$child->save($save_child_data);


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {


        if (!checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_child_data = array();
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {


                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);


                $this->$parent->update($save_parent_data, $update_by);
                $save_child_data['Title'] = $post_data['Title'];

                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);

            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    $save_child_data['Title'] = $post_data['Title'];


                    $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->update($save_child_data, $update_by);

                } else {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->save($save_child_data);
                }


            }

            $this->Temp_order_model->update(array('IsEdited' => 1), array('ProductID' => $post_data[$this->data['TableKey']]));

            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(61, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->Product_image_model->delete($deleted_by);
        $this->Product_like_model->delete($deleted_by);
        $this->Product_comment_model->delete($deleted_by);
        $this->Product_reported_model->delete($deleted_by);
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    public function updateOutOfStock()
    {
        $post_data = $this->input->post();
        $update['OutOfStock'] = $post_data['OutOfStock'];
        $update_by['ProductID'] = $post_data['ProductID'];
        $this->Product_model->update($update, $update_by);
        $this->Temp_order_model->update(array('IsEdited' => 1), array('ProductID' => $post_data['ProductID']));
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;
    }

    public function updateIsActive()
    {
        $post_data = $this->input->post();
        $update['IsActive'] = $post_data['IsActive'];
        $update_by['ProductID'] = $post_data['ProductID'];
        $this->Product_model->update($update, $update_by);
        $this->Temp_order_model->update(array('IsEdited' => 1), array('ProductID' => $post_data['ProductID']));
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;
    }

    public function approvePromotion()
    {
        $post_data = $this->input->post();
        $update['IsPromotionApproved'] = 1;
        $update_by['ProductID'] = $post_data['ProductID'];
        $this->Product_model->update($update, $update_by);
        $this->sendNotificationToUser($update_by['ProductID']);
        $success['error'] = false;
        $success['success'] = "Product promotion approved successfully.";
        echo json_encode($success);
        exit;
    }

    private function sendNotificationToUser($ProductID)
    {
        $product = $this->Product_model->getProducts("products.ProductID = " . $ProductID, $this->language);
        if ($product) {
            $message = "Your promotion request is approved for your product titled '" . $product[0]->Title . "'";
            $res = sendNotification('Booth', $message, array(), $product[0]->UserID);
        }
    }


}