<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('UTC');

class Dashboard extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->load->model('Dashboard_model');
        $this->load->model('Order_model');
        $this->load->model('Booking_model');
        $this->load->model('User_model');
    }

    public function index($year = '')
    {
        if (!checkUserRightAccess(69, $this->session->userdata['admin']['UserID'], 'CanView')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/category'));
        }
        if($year == ''){
            $year = date('Y');
        }
        $this->data['year'] = $year;

        // Total Sales Chart Data
        $TotalSales = $this->Order_model->getTotalsales($year);
        $this->data['TotalSales'] = array();
        if(!empty($TotalSales)){
            $TotalSales = array_column($TotalSales, 'TotalSales','SalesMonth');
            for($i=0;$i<=11;$i++){
                if(isset($TotalSales[$i+1])){
                    $this->data['TotalSales'][$i] =  $TotalSales[$i+1];
                }else{
                    $this->data['TotalSales'][$i] =  0;
                }
            }
        }

        // Total Orders Chart Data
        $TotalOrdersInMonths = $this->Order_model->getTotalOrders($year);
        $this->data['TotalOrdersInMonths'] = array();
        if(!empty($TotalOrdersInMonths)){
            $TotalOrdersInMonths = array_column($TotalOrdersInMonths, 'TotalOrders','OrderMonth');
            for($i=0;$i<=11;$i++){
                if(isset($TotalOrdersInMonths[$i+1])){
                    $this->data['TotalOrdersInMonths'][$i] =  $TotalOrdersInMonths[$i+1];
                }else{
                    $this->data['TotalOrdersInMonths'][$i] =  0;
                }
            }
        }

        // dump($this->data['TotalOrdersInMonths']);

        $this->data['TotalOrdersCount'] = $this->Dashboard_model->getTotalOrders();
        $this->data['TodayOrdersCount'] = $this->Dashboard_model->getTodaysOrders();
        $this->data['WeeklyOrdersCount'] = $this->Dashboard_model->getWeeklyOrders();
        $this->data['MonthlyOrdersCount'] = $this->Dashboard_model->getMonthlyOrders();
        $this->data['bookings_count'] = 0;
        $this->data['bookings_categories_count'] = $this->Dashboard_model->getMostOrdersByCategory();
        $this->data['total_sales'] = $this->Dashboard_model->getTotalSales();
        $this->data['monthly_sales'] = $this->Dashboard_model->getMonthlySales();
        $this->data['weekly_sales'] = $this->Dashboard_model->getWeeklySales();
        $this->data['todays_sales'] = $this->Dashboard_model->getTodaysSales();
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    private function allOrders()
    {
        $language = 'EN';
        $new_orders = $this->Booking_model->getBookings(false, false, false, 0, $language);
        return $new_orders;
    }

    private function bookingsForToday()
    {
        $language = 'EN';
        $Date_timestamp = time();
        $Date = date('Y-m-d', $Date_timestamp);
        $where_scheduled = "DATE_FORMAT(FROM_UNIXTIME(bookings.BookingTime), '%Y-%m-%d') = '$Date'";
        $scheduled_orders = $this->Booking_model->getBookings($where_scheduled, true, false, 0, $language);
        return $scheduled_orders;
    }

    private function getCompletedOrders()
    {
        $language = 'EN';
        $where_completed = "bookings.Status = 5";
        $completed_orders = $this->Booking_model->getBookings($where_completed, false, false, 0, $language);
        return $completed_orders;
    }

    private function getOverdueOrders()
    {
        $language = 'EN';
        $where_overdue = "bookings.Status = 7";
        $overdue_bookings = $this->Booking_model->getBookings($where_overdue, false, false, 0, $language);
        return $overdue_bookings;
    }

}