<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Theme extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model',
            'Model_general'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'ThemeID';
        $this->data['Table'] = 'themes';


    }


    public function index()
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language);

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {

        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = array();
        $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }


        $save_parent_data['Color'] = $post_data['Color'];
        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);


        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

        if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {
            $save_parent_data['Image'] = uploadImage("Image", "uploads/");
        }


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {


            $default_lang = getDefaultLanguage();


            $save_child_data['Title'] = $post_data['Title'];
            $save_child_data[$this->data['TableKey']] = $insert_id;
            $save_child_data['SystemLanguageID'] = $default_lang->SystemLanguageID;
            $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
            $this->$child->save($save_child_data);


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {


        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_child_data = array();
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {

                $save_parent_data['Color'] = $post_data['Color'];
                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {
                    $save_parent_data['Image'] = uploadImage("Image", "uploads/");
                }
                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);


                $this->$parent->update($save_parent_data, $update_by);
                $save_child_data['Title'] = $post_data['Title'];

                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);

            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    $save_child_data['Title'] = $post_data['Title'];


                    $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->update($save_child_data, $update_by);

                } else {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->save($save_child_data);
                }


            }

            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->Model_general->deleteWhere('theme_styles', array('ThemeID' => $this->input->post('id')));
        $this->Model_general->deleteWhere('user_customization', array('ThemeID' => $this->input->post('id')));
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    public function tops($main_image_id)
    {
        $parent = $this->data['Parent_model'];
        $this->data['main_image_id'] = $main_image_id;
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/tops';
        $this->data['theme'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $main_image_id, 'DESC', '');
        $this->data['results'] = $this->Model_general->getMultipleRows('theme_styles', array('ThemeID' => $main_image_id, 'ParentID' => 0), false, 'asc', 'ThemeStyleID');
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function addTop($main_image_id)
    {
        $this->data['main_image_id'] = $main_image_id;
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/addTop';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function saveTop()
    {
        $post_data = $this->input->post();
        if (isset($_FILES['StyleImage']) && $_FILES['StyleImage']['name'] != '') {
            $post_data['StyleImage'] = uploadImage("StyleImage", "uploads/");
        }
        $insert_id = $this->Model_general->save('theme_styles', $post_data);
        if ($insert_id > 0)
        {
            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['reload'] = true;
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

    }

    public function bottoms($main_image_id, $parent_id)
    {
        $parent = $this->data['Parent_model'];
        $this->data['main_image_id'] = $main_image_id;
        $this->data['parent_id'] = $parent_id;
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/bottoms';
        $this->data['result'] = $this->Model_general->getSingleRow('theme_styles', array('ThemeStyleID' => $parent_id));
        $this->data['results'] = $this->Model_general->getMultipleRows('theme_styles', array('ThemeID' => $main_image_id, 'ParentID' => $parent_id), false, 'asc', 'ThemeStyleID');
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function addBottom($main_image_id, $parent_id)
    {
        $this->data['main_image_id'] = $main_image_id;
        $this->data['parent_id'] = $parent_id;
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/addBottom';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function saveBottom()
    {
        $post_data = $this->input->post();
        if (isset($_FILES['StyleImage']) && $_FILES['StyleImage']['name'] != '') {
            $post_data['StyleImage'] = uploadImage("StyleImage", "uploads/");
        }
        $insert_id = $this->Model_general->save('theme_styles', $post_data);
        if ($insert_id > 0)
        {
            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['reload'] = true;
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

    }

    public function editStyle($style_id)
    {
        $this->data['result'] = $this->Model_general->getSingleRow('theme_styles', array('ThemeStyleID' => $style_id));
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/editStyle';
        $this->data['style_id'] = $style_id;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function updateStyle()
    {
        $post_data = $this->input->post();
        $ThemeStyleID = $post_data['ThemeStyleID'];
        unset($post_data['ThemeStyleID']);
        if (isset($_FILES['StyleImage']) && $_FILES['StyleImage']['name'] != '') {
            $post_data['StyleImage'] = uploadImage("StyleImage", "uploads/");
        }
        $this->Model_general->updateWhere('theme_styles', $post_data, array('ThemeStyleID' => $ThemeStyleID));
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        $success['reload'] = true;
        echo json_encode($success);
        exit;
    }


}