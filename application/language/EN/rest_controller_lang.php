<?php

/*
 * English language
 */

//General

$lang['some_thing_went_wrong'] = 'There is something went wrong';


$lang['save_successfully'] = 'Saved Successfully';
$lang['update_successfully'] = 'Updated Successfully';
$lang['deleted_successfully'] = 'Deleted Successfully';
$lang['you_should_update_data_seperately_for_each_language'] = 'You would need to update data separately for each language';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['actions'] = 'Action';
$lang['submit'] = 'Submit';
$lang['back'] = 'Back';
$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['are_you_sure'] = 'Are you sure you want to delete this?';
$lang['view'] = 'View';
$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['is_active'] = 'Is Active';
$lang['create_table'] = 'Create Table';
$lang['create_model'] = 'Create Model';
$lang['create_view'] = 'Create View';
$lang['create_controller'] = 'Create Controller';
$lang['logout'] = 'Logout';

$lang['please_add_role_first'] = 'Please add role first';
$lang['you_dont_have_its_access'] = 'You don\'t have its access';

$lang['SiteName'] = 'Site Name';
$lang['PhoneNumber'] = 'Phone Number';
$lang['Whatsapp'] = 'Whatsapp';
$lang['Skype'] = 'Skype';
$lang['Fax'] = 'Fax';
$lang['SiteLogo'] = 'Site Logo';
$lang['EditSiteSettings'] = 'Edit Site Settings';
$lang['FacebookUrl'] = 'Facebook Url';
$lang['GoogleUrl'] = 'Google Url';
$lang['LinkedInUrl'] = 'LinkedIn Url';
$lang['TwitterUrl'] = 'Twitter Url';
//end General

// Module Section
$lang['choose_parent_module'] = 'Choose Parent Module';
$lang['parent_module'] = 'Parent Module';
$lang['slug'] = 'Slug';
$lang['icon_class'] = 'Icon Class';

$lang['title'] = 'Title';
$lang['add_module'] = 'Add Module';
$lang['modules'] = 'Modules';
$lang['module'] = 'Module';
$lang['module_rights'] = 'Module Rights';

$lang['please_add_module_first'] = 'Please add module first';
$lang['rights'] = 'Rights';


// Roles Section

$lang['select_role'] = 'Select Role';
$lang['add_role'] = 'Add Role';
$lang['roles'] = 'Roles';
$lang['role'] = 'Role';
$lang['please_add_role_first'] = 'Please add role first';
$lang['choose_user_role'] = 'Choose user role';


// activity section

$lang['add_activity'] = 'Add Activity';
$lang['activities'] = 'Activities';
$lang['activity'] = 'Activity';
$lang['choose_activity'] = 'Choose Activity';

// user

$lang['please_add_user_first'] = 'Please add user first';

$lang['name'] = 'Name';
$lang['user'] = 'User';
$lang['users'] = 'Users';
$lang['add_user'] = 'Add User';
$lang['select_user'] = 'Select User';

$lang['email'] = 'Email';
$lang['password'] = 'Password';
$lang['min_length'] = '(Min char 8)';
$lang['confirm_password'] = 'Confirm Password';


$lang['add_city'] = 'Add City';
$lang['citys'] = 'Citys';
$lang['city'] = 'City';
$lang['add_district'] = 'Add District';
$lang['districts'] = 'Districts';
$lang['district'] = 'District';
$lang['please_add_distric_first'] = 'Please add distric first';
$lang['choose_district'] = 'Choose District';
$lang['add_patient'] = 'Add Patient';
$lang['patients'] = 'Patients';
$lang['patient'] = 'Patient';
$lang['add_center'] = 'Add center';
$lang['centers'] = 'Centers';
$lang['center'] = 'Center';
$lang['add_screening_camp'] = 'Add Screening camp';
$lang['screening_camps'] = 'Screening camps';
$lang['screening_camp'] = 'Screening camp';
$lang['add_center_type'] = 'Add Center type';
$lang['center_types'] = 'Center Types';
$lang['center_type'] = 'Center Type';
$lang['add_email_template'] = 'Add Email Template';
$lang['email_templates'] = 'Email Templates';
$lang['email_template'] = 'Email Template';
$lang['Heading'] = 'Heading';
$lang['Description'] = 'Description';
$lang['Image'] = 'Image';
$lang['add_language'] = 'Add Language';
$lang['languages'] = 'Languages';
$lang['language'] = 'Language';
$lang['short_code'] = 'Short Code';

$lang['is_default'] = 'Is Default';
$lang['please_select_default_value_language_first'] = 'Please select another default value language first';
$lang['add_test'] = 'Add Test';
$lang['tests'] = 'Tests';
$lang['test'] = 'Test';
$lang['add_test'] = 'Add Test';
$lang['tests'] = 'Tests';
$lang['test'] = 'Test';
$lang['add_group'] = 'Add Group';
$lang['groups'] = 'Groups';
$lang['group'] = 'Group';
$lang['add_group'] = 'Add Group';
$lang['groups'] = 'Groups';
$lang['group'] = 'Group';
$lang['add_feed'] = 'Add Feed';
$lang['feeds'] = 'Feeds';
$lang['feed'] = 'Feed';
$lang['add_category'] = 'Add Category';
$lang['categorys'] = 'Categories';
$lang['category'] = 'Category';
$lang['add_child_category'] = 'Add Child Category';
$lang['child_catagories_of'] = 'Child Catagories Of';
$lang['add_postReported'] = 'Add PostReported';
$lang['postReporteds'] = 'PostReporteds';
$lang['postReported'] = 'PostReported';
$lang['add_repotedComment'] = 'Add RepotedComment';
$lang['repotedComments'] = 'RepotedComments';
$lang['repotedComment'] = 'RepotedComment';
$lang['add_repotedGroup'] = 'Add RepotedGroup';
$lang['repotedGroups'] = 'RepotedGroups';
$lang['repotedGroup'] = 'RepotedGroup';
$lang['add_type'] = 'Add Type';
$lang['types'] = 'Types';
$lang['type'] = 'Type';
$lang['add_user_type'] = 'Add User_type';
$lang['user_types'] = 'User_types';
$lang['user_type'] = 'User_type';
//////////////////
$lang['something_went_wrong'] = 'Something went wrong. Please try again later.';
$lang['registered_successfully'] = 'Registered successfully';
$lang['username'] = 'Username';
$lang['mobile'] = 'Mobile No';
$lang['sorry_you_cant_login'] = 'Sorry! You can not login at this moment. Please contact admin for further details.';
$lang['logged_in_successfully'] = 'Logged in successfully.';
$lang['user_not_found_with_these_login_details'] = 'User not found with these login details.';
$lang['user_not_found'] = 'User not found.';
$lang['email_already_exist'] = 'Email address already exists.';
$lang['username_already_exist'] = 'Username already exists.';
$lang['booth_username_already_exist'] = 'Booth username already exists.';
$lang['mobile_already_exist'] = 'Mobile no. already exists.';
$lang['user_logged_out_successfully'] = 'User logged out successfully.';
$lang['password_reset_link_sent_in_email'] = 'Password reset link is sent to you on your registered email.';
$lang['password_reset_link_sent_in_mobile'] = 'Password reset link is sent to you on your registered mobile no.';
$lang['update_available_for_app'] = 'An update is available to your app.';
$lang['app_version_needed'] = 'App version not sent.';
$lang['followed_successfully'] = 'Followed successfully.';
$lang['unfollowed_successfully'] = 'Unfollowed successfully.';
$lang['add_product'] = 'Add Product';
$lang['products'] = 'Products';
$lang['product'] = 'Product';
$lang['please_verify_your_email'] = 'Your email address is not verified. Please verify it to proceed.';
$lang['please_verify_your_mobile'] = 'Your mobile no is not verified. Please verify it to proceed.';
$lang['verification_code_sent'] = 'Verification code is sent to your mobile no.';
$lang['your_mobile_already_verified'] = 'Your mobile number is already verified.';
$lang['mobile_no_verified'] = 'Your mobile no is verified successfully.';
$lang['mobile_no_failed_to_verified'] = 'Your mobile no verification has failed.';
$lang['profile_updated'] = 'Your profile is updated successfully.';
$lang['profile_updated_interest'] = 'Your profile is updated successfully.';
$lang['notification_marked_as_read'] = 'Notification marked as read.';
$lang['product_added_successfully'] = 'Product added successfully.';
$lang['product_updated_successfully'] = 'Product updated successfully.';
$lang['booth_name'] = 'Booth Name';
$lang['product_type'] = 'Product Type';
$lang['description'] = 'Description';
$lang['add_theme'] = 'Add Theme';
$lang['themess'] = 'Themes';
$lang['themes'] = 'Themes';
$lang['theme'] = 'Theme';
$lang['user_already_reported'] = 'User is already reported.';
$lang['user_reported_successfully'] = 'User reported successfully.';
$lang['user_already_blocked'] = 'User is already blocked.';
$lang['user_blocked_successfully'] = 'User blocked successfully.';
$lang['user_unblocked_successfully'] = 'User unblocked successfully.';
$lang['profile_customized_successfully'] = 'Profile customized successfully.';
$lang['feedback_sent_successfully'] = 'Feedback sent successfully.';
$lang['add_feedback'] = 'Add Feedback';
$lang['feedbacks'] = 'Feedbacks';
$lang['feedback'] = 'Feedback';
$lang['price'] = 'Price';
$lang['currency'] = 'Currency';
$lang['out_of_stock'] = 'Out of stock';
$lang['product_already_reported'] = 'Product is already reported.';
$lang['product_reported_successfully'] = 'Product reported successfully.';
$lang['product_disliked_successfully'] = 'Product disliked successfully.';
$lang['product_liked_successfully'] = 'Product liked successfully.';
$lang['product_already_liked'] = 'Product is already liked.';
$lang['comment_saved_successfully'] = 'Comment saved successfully';
$lang['added_to_wishlist'] = 'Product added to wishlist.';
$lang['removed_from_wishlist'] = 'Product removed from wishlist.';
$lang['added_to_cart'] = 'Product added to cart.';
$lang['cart_updated'] = 'Cart updated successfully.';
$lang['product_not_available'] = 'Sorry! This product is not available.';
$lang['question_already_reported'] = 'Question is already reported.';
$lang['question_reported_successfully'] = 'Question reported successfully.';
$lang['comment_already_reported'] = 'Comment is already reported.';
$lang['comment_notfound'] = 'Comment not found.';
$lang['comment_reported_successfully'] = 'Comment reported successfully.';
$lang['answear_comment_reported_successfully'] = 'Answear comment reported successfully.';
$lang['answear_comment_notfound'] = 'Answear comment not found.';
$lang['answear_comment_already_reported'] = 'Answear comment is already reported.';
$lang['your_cart_is_empty'] = 'Your cart is empty. Please add items to your cart first.';
$lang['wrong_old_password'] = 'Wrong old password provided.';
$lang['password_changed_successfully'] = 'Password changed successfully. Please use the new password next time you login.';
$lang['your_order_sent_for_approval'] = 'Your order has been sent!  You will be notified once Booth approves your order.
';
$lang['your_order_sent_for_approval_removed_products'] = 'Thank you for placing your order with us. Some of the products in your order are not available right now. You will be notified once relevant booths approve your ordered items.';
$lang['order_approved'] = 'Order approved successfully.';
$lang['order_payment_done'] = 'Order confirmed successfully.';
$lang['order_dispatched'] = 'Order dispatched successfully.';
$lang['order_completed'] = 'Order completed successfully.';
$lang['order_cancelled'] = 'Order cancelled successfully.';
$lang['order_cant_be_cancelled'] = 'Sorry! You can not cancel this order now.';
$lang['order_status_cant_be_changed'] = 'Order status can not be changed.';
$lang['order_status_cant_be_changed_wrong_code'] = 'Please provide the correct delivery code.';
$lang['ticket_already_raised'] = 'Ticket is already raised against this order request.';
$lang['ticket_raised'] = 'Ticket is raised successfully.';
$lang['add_country'] = 'Add Country';
$lang['countrys'] = 'Countries';
$lang['country'] = 'Country';
$lang['chat_room_cant_be_deleted'] = 'Chat room can not be deleted as there are messages in this chat room.';
$lang['chat_room_deleted'] = 'Chat room deleted successfully.';
$lang['invitation_sent'] = 'Invitation sent successfully.';
$lang['invitation_already_sent_mobile'] = 'Invitation is already sent to this mobile no.';
$lang['invitation_already_sent_email'] = 'Invitation is already sent to this email address.';
$lang['user_already_exist_with_mobile'] = 'User already exist against this mobile no.';
$lang['user_already_exist_with_email'] = 'User already exist against this email address.';
$lang['coupon_applied'] = 'Promo code applied successfully.';
$lang['coupon_expired'] = 'Promo code is expired.';
$lang['coupon_not_available_for_seller'] = 'This promo code is not available for this seller.';
$lang['coupon_invalid'] = 'Promo code is invalid.';
$lang['product_promoted'] = 'Product is reposted successfully';//'Product promotion request send to admin. Admin will review your request and approve.';
$lang['already_added_to_wishlist'] = 'Already added to wishlist.';
$lang['product_already_promoted_for_store'] = 'One of your products is already on promotion.';
$lang['promoted_product_not_found'] = 'No promoted product found.';
$lang['buyer_already_rated'] = 'Sorry! This buyer is already rated.';
$lang['seller_already_rated'] = 'Sorry! This Seller is already rated.';
$lang['buyer_rated'] = 'Thank you for rating this buyer.';
$lang['seller_rated'] = 'Thank you for rating this seller.';
$lang['sorry_cant_add_product_to_cart'] = 'Sorry! You can not add this product to cart.';
$lang['product_already_rated'] = 'You have already rated this product.';
$lang['product_rated'] = 'Thank you for rating this product.';
$lang['coupon_created_successfully'] = 'Promocode created successfully.';
$lang['coupon_updated_successfully'] = 'Promocode updated successfully.';
$lang['coupon_deleted_successfully'] = 'Promocode deleted successfully.';
$lang['question_deleted_successfully'] = 'Question deleted successfully';
$lang['answear_deleted_successfully'] = 'Answear deleted successfully';
$lang['please_rate_booth_first'] = 'Please rate your last completed order to place a new order.';
$lang['coupon_must_contain_unique_value'] = 'Promocode must cotain unique value';
$lang['already_complained'] = 'You have already complained aginst this order';
$lang['promocode_limit'] = 'Yor promocode limit completed';
$lang['product_is_in_cart'] = 'You can\'t delete this prodcut.It is in order state';
$lang['same_password'] = 'Both old and new passwords are same.';
$lang['cannot_send_message'] = 'You can\'t send the message.';
$lang['you_cannot_do'] = 'You can\'t do this activity.';
$lang['already_login_some'] = 'You are already logged In on another device. In case you can\'t access to that device you can logged In by using forgot password.';

$lang['add_package']            = 'Add Package';
$lang['packages']            = 'Packages';
$lang['package']            = 'Package';
$lang['email_not_verify']            = 'Please verify your email first';
$lang['mobile_no_not_verify']            = 'Please verify your mobile no first';
$lang['post_added_successfully']            = 'Post added successfully';
$lang['post_reported_successfully'] = 'Post reported successfully.';
$lang['product_already_reported'] = 'Post is already reported.';
$lang['please_update_your_package'] = 'Account is no longer active.';